param([Int32] $day)

$template = @"
#[aoc(day$day, part1)]
pub fn part1(input: &str) -> usize {
    1
}

#[aoc(day$day, part2)]
pub fn part2(input: &str) -> usize {
    1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1(""), 1)
    }

    #[test]
    fn sample2() {
        assert_eq!(part2(""), 1)
    }
}
"@

if ($day -le 9) {
    $dayName = "0$day"
}
else {
    $dayName = "$day"
}
Set-Content src/day$dayName.rs $template
(Get-Content src/lib.rs -Raw) -replace "(?ms)\r?\n\r?\naoc", "
pub mod day$dayName;

aoc" | Set-Content -NoNewline src/lib.rs

git add src/lib.rs
git add src/day$dayName.rs

cargo aoc input -d $day -y 2023