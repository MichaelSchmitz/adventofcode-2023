use std::collections::{HashMap, HashSet};
use std::ops::Range;
use itertools::Itertools;
use regex::Regex;

#[derive(Clone)]
struct Brick {
    x: Range<i32>,
    y: Range<i32>,
    z: Range<i32>
}

pub struct Graph {
    nodes: HashSet<usize>,
    edges: HashSet<(usize, usize)>
}

#[aoc_generator(day22)]
pub fn gen_bricks(input: &str) -> Graph {
    let mut bricks = Vec::new();
    let regex = Regex::new(r"(\d*),(\d*),(\d*)").unwrap();
    for line in input.lines() {
        let (start, end) = line.split_once('~').unwrap();
        let Some((_, [x1, y1, z1])) = regex.captures(start).map(|c| c.extract()) else { unreachable!() };
        let Some((_, [x2, y2, z2])) = regex.captures(end).map(|c| c.extract()) else { unreachable!() };
        bricks.push(Brick {
            x: x1.parse::<i32>().unwrap()..x2.parse::<i32>().unwrap()+1,
            y: y1.parse::<i32>().unwrap()..y2.parse::<i32>().unwrap()+1,
            z: z1.parse::<i32>().unwrap()..z2.parse::<i32>().unwrap()+1,
        });
    }
    bricks.sort_by_key(|brick| brick.z.start);
    let mut resting = HashMap::new();

    let mut supports = Graph {
        nodes: HashSet::new(),
        edges: HashSet::new()
    };
    for (id, &ref brick) in bricks.iter().enumerate() {
        let mut target = brick.clone();
        let mut below = get_below(&brick);
        while !is_intersecting(&below, &resting) {
            target = below.clone();
            below = get_below(&below);
        }

        supports.nodes.insert(id);
        for p in get_all_points(&below) {
            let rest = resting.get(&p);
            if rest.is_some() {
                supports.edges.insert((*rest.unwrap(), id));
            }
        }

        for p in get_all_points(&target) {
            resting.insert(p, id);
        }
    }


    supports
}

fn get_below(b: &Brick) -> Brick {
    Brick {
        x: b.x.clone(),
        y: b.y.clone(),
        z: b.z.start-1..b.z.end-1
    }
}

fn is_intersecting(a: &Brick, resting: &HashMap<(i32, i32, i32), usize>) -> bool {
    a.z.start <= 0 || get_all_points(a).iter().any(|p| resting.contains_key(p))
}

fn get_all_points(b: &Brick) -> Vec<(i32, i32, i32)> {
    let mut points = Vec::new();
    for x in b.x.clone() {
        for y in b.y.clone() {
            for z in b.z.clone() {
                points.push((x, y, z));
            }
        }
    }
    points
}

#[aoc(day22, part1)]
pub fn part1(input: &Graph) -> usize {
    let mut candidates = 0;

    for node in &input.nodes {
        if input.edges.iter().filter(|(from, _)| from == node)
            .all(|(_, n)| input.edges.iter().filter(|(_, to)| to == n).count() > 1) {
            candidates += 1;
        }
    }

    candidates
}

#[aoc(day22, part2)]
pub fn part2(input: &Graph) -> usize {
    let mut sum = 0;

    for node in &input.nodes {
        let mut falling = HashSet::new();
        falling.insert(*node);
        let mut supported = input.edges.iter().filter(|(from, _)| from == node).collect_vec();
        while let Some(support) = supported.pop() {
            if input.edges.iter().filter(|(_, to)| to == &support.1).all(|(from, _)| falling.contains(from)) {
                falling.insert(support.1);
                for e in input.edges.iter().filter(|(from, _)| from == &support.1).collect_vec() {
                    supported.push(e);
                }
            }
        }
        sum += falling.len() - 1;
    }

    sum
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1(&gen_bricks("1,0,1~1,2,1
0,0,2~2,0,2
0,2,3~2,2,3
0,0,4~0,2,4
2,0,5~2,2,5
0,1,6~2,1,6
1,1,8~1,1,9")), 5)
    }

    #[test]
    fn sample2() {
        assert_eq!(part2(&gen_bricks("1,0,1~1,2,1
0,0,2~2,0,2
0,2,3~2,2,3
0,0,4~0,2,4
2,0,5~2,2,5
0,1,6~2,1,6
1,1,8~1,1,9")), 7)
    }
}
