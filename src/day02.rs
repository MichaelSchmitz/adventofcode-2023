#[aoc(day2, part1)]
pub fn part1(input: &str) -> u32 {
    let mut possible_games: u32 = 0;
    for line in input.lines() {
        let game = line.split(": ").collect::<Vec<&str>>();
        if play_game(game[1].trim())
        {
            possible_games += game[0].split(' ').last().unwrap().parse::<u32>().unwrap()
        }
    }

    possible_games
}

fn play_game(game: &str) -> bool {
    for subgame in game.split("; ") {
        let cubes: Vec<&str> = subgame.split(", ").collect();
        for cube in cubes
        {
            let color_count: Vec<&str>  = cube.split(' ').collect();
            match color_count[1] {
                "green" => {
                    if color_count[0].parse::<u32>().is_ok_and(|g| g > 13) {
                        return false;
                    }
                },
                "blue" => {
                    if color_count[0].parse::<u32>().is_ok_and(|g| g > 14) {
                        return false;
                    }
                },
                "red" => {
                    if color_count[0].parse::<u32>().is_ok_and(|g| g > 12) {
                        return false;
                    }
                },
                _ => {
                    println!("{}", color_count[1]);
                    unreachable!()
                }
            }
        }
    }

    true
}

#[aoc(day2, part2)]
pub fn part2(input: &str) -> u32 {
    let mut power_of_cubes: u32 = 0;
    for line in input.lines() {
        let game = line.split(": ").collect::<Vec<&str>>();
        power_of_cubes += minimum_play_game(game[1].trim());
    }

    power_of_cubes
}


fn minimum_play_game(game: &str) -> u32 {
    let mut red: u32 = 0;
    let mut green: u32 = 0;
    let mut blue: u32 = 0;
    for subgame in game.split("; ") {
        let cubes: Vec<&str> = subgame.split(", ").collect();
        for cube in cubes
        {
            let color_count: Vec<&str>  = cube.split(' ').collect();
            match color_count[1] {
                "green" => {
                    let new_green = color_count[0].parse::<u32>().unwrap();
                    if new_green > green
                    {
                        green = new_green
                    }
                },
                "blue" => {
                    let new_blue = color_count[0].parse::<u32>().unwrap();
                    if new_blue > blue
                    {
                        blue = new_blue
                    }
                },
                "red" => {
                    let new_red = color_count[0].parse::<u32>().unwrap();
                    if new_red > red
                    {
                        red = new_red
                    }
                },
                _ => {
                    println!("{}", color_count[1]);
                    unreachable!()
                }
            }
        }
    }

    red * green * blue
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"), 1)
    }
    #[test]
    fn sample2() {
        assert_eq!(part1("Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue"), 2)
    }
    #[test]
    fn sample3() {
        assert_eq!(part1("Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red"), 0)
    }
    #[test]
    fn sample4() {
        assert_eq!(part1("Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red"), 0)
    }
    #[test]
    fn sample5() {
        assert_eq!(part1("Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"), 5)
    }
    #[test]
    fn sample6() {
        assert_eq!(part1("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"), 8)
    }
    #[test]
    fn sample7() {
        assert_eq!(part2("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"), 48)
    }
    #[test]
    fn sample8() {
        assert_eq!(part2("Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue"), 12)
    }
    #[test]
    fn sample9() {
        assert_eq!(part2("Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red"), 1560)
    }
    #[test]
    fn sample10() {
        assert_eq!(part2("Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red"), 630)
    }
    #[test]
    fn sample11() {
        assert_eq!(part2("Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"), 36)
    }
    #[test]
    fn sample12() {
        assert_eq!(part2("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"), 2286)
    }
}