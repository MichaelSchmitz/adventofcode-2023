use std::collections::HashMap;
use std::str::from_utf8_unchecked;
use itertools::Itertools;
use crate::day14::Type::{Empty, Movable, Static};

type Entry = Vec<Type>;

type Entries = Vec<Entry>;

#[derive(Eq, PartialEq, Copy, Clone)]
enum Type {
    Static,
    Movable,
    Empty
}

fn gen_map(input: &str) -> Entries {
    let mut entries: Vec<Entry> = Vec::new();
    for _ in 0..input.find('\n').unwrap() {
        entries.push(vec![])
    }
    for  line in input.lines() {
        for (i, c) in line.chars().enumerate() {
            entries[i].push(match c {
                'O' => Movable,
                '#' => Static,
                '.' => Empty,
                _ => unreachable!()
            })
        }
    }

    entries
}

#[aoc(day14, part1)]
pub fn part1(input: &str) -> usize {
    let map = gen_map(input);
    let mut load = 0;
    for column in map {
        let inital_max_load = column.len();
        let mut max_load = inital_max_load;
        for (i, pos) in column.iter().enumerate() {
            if *pos == Empty {
                continue;
            }
            else if *pos == Movable {
                load += max_load;
                max_load -= 1;
            }
            else if *pos == Static {
                max_load = inital_max_load - i - 1;
            }
        }
    }

    load
}

fn simulate_north(input: &mut [u8], rows: usize, cols: usize){
    let mut row_stops = vec![0; cols];
    let mut pos = 0;
    for row in 0..rows {
        for col in 0..cols {
            let c = input[pos];
            match c {
                b'O' => {
                    input[pos] = b'.';
                    input[row_stops[col] * cols + col] = b'O';
                    row_stops[col] += 1;
                }
                b'#' => {
                    row_stops[col] = row + 1;
                }
                _ => {}
            }
            pos += 1;
        }
    }
}

fn simulate_south(input: &mut [u8], rows: usize, cols: usize)  {
    let mut row_stops = vec![rows - 1; cols];

    let mut pos = rows * cols - 1;
    for row in (0..rows).rev() {
        for col in (0..cols).rev() {
            let c = input[pos];
            match c {
                b'O' => {
                    input[pos] = b'.';
                    input[row_stops[col] * cols + col] = b'O';
                    row_stops[col] = row_stops[col].saturating_sub(1);
                }
                b'#' => {
                    row_stops[col] = row.saturating_sub(1);
                }
                _ => { }
            }
            pos = pos.saturating_sub(1);
        }
    }
}

fn simulate_east(input: &mut [u8], rows: usize, cols: usize) {
    let mut pos = rows * cols - 1;
    let mut row_start = rows * cols - cols;
    for _ in (0..rows).rev() {
        let mut stop = cols - 1;
        for col in (0..cols).rev() {
            let c = input[pos];
            match c {
                b'O' => {
                    input[pos] = b'.';
                    input[row_start + stop - 1] = b'O';
                    stop = stop.saturating_sub(1);
                }
                b'#' => {
                    stop = col;
                }
                _ => { }
            }
            pos = pos.saturating_sub(1);
        }
        row_start = row_start.saturating_sub(cols);
    }
}

fn simulate_west(input: &mut [u8], rows: usize, cols: usize) {
    let mut pos = 0;
    let mut row_start = 0;
    for _ in 0..rows {
        let mut stop = 0;
        for col in 0..cols {
            let c = input[pos];
            match c {
                b'O' => {
                    input[pos] = b'.';
                    input[row_start + stop] = b'O';
                    stop += 1;
                }
                b'#' => {
                    stop = col + 1;
                }
                _ => { }
            }
            pos += 1;
        }
        row_start += cols;
    }
}


#[aoc(day14, part2)]
pub fn part2(input: &[u8]) -> usize {
    let mut vec = input.to_owned();
    vec.push(b'\n');
    let map = vec.as_mut_slice();
    let rows = input.iter().find_position(|c| **c == b'\n').unwrap().0;
    let cols = rows + 1;
    let mut cache: HashMap<String, usize> = HashMap::new();
    let mut count = 0;
    loop {
        simulate_north(map, rows, cols);
        simulate_west(map, rows, cols);
        simulate_south(map, rows, cols);
        simulate_east( map, rows, cols);
        count += 1;
        let dup = cache.insert(unsafe { from_utf8_unchecked(map).to_owned() }, count);
        if dup.is_some() {
            let remaining = (1_000_000_000 - count) % (count - dup.unwrap());
            for _ in 0..remaining {
                simulate_north(map, rows, cols);
                simulate_west(map, rows, cols);
                simulate_south(map, rows, cols);
                simulate_east(map, rows, cols);
            }

            return calc_load(map, rows)
        }
    }
}


pub fn calc_load(input: &[u8], size: usize) -> usize {
    let mut total_load = 0;

    for (line, load) in input.chunks_exact(size + 1).zip((1..size + 1).rev()) {
        for c in line {
            match *c {
                b'O' => {
                    total_load += load;
                }
                _ => {}
            }
        }
    }

    total_load
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn sample1() {
        assert_eq!(part1("O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#...."), 136)
    }

    #[test]
    fn sample2() {
        assert_eq!(part2(& "O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....".as_bytes()), 64)
    }
}
