use std::collections::HashMap;

#[derive(Eq, PartialEq, Copy, Clone, Hash)]
struct Position {
    x: i32,
    y: i32
}

impl Position {
    fn next(&self, count: i32, dir: Direction) -> Position {
        match dir {
            Direction::Right => Position { x: self.x + count, y: self.y },
            Direction::Left => Position { x: self.x - count, y: self.y },
            Direction::Up => Position { x: self.x, y: self.y - count },
            Direction::Down => Position { x: self.x, y: self.y + count }
        }
    }

    fn manhattan(&self, other: Position) -> usize {
        ((self.x - other.x).abs() + (self.y - other.y).abs()) as usize
    }
}

#[derive(Eq, PartialEq, Clone, Copy)]
enum Direction {
    Left, Right, Up, Down
}

impl Direction {
    fn index(&self) -> usize {
        match self {
            Direction::Left => 0,
            Direction::Right => 1,
            Direction::Up => 2,
            Direction::Down => 3
        }
    }
}

#[aoc(day17, part1)]
pub fn part1(input: &str) -> usize {
    let map = gen_map(input);

    a_star(map, 1, 3)
}

fn gen_map(input: &str) -> HashMap<Position, usize> {
    let mut map: HashMap<Position, usize> = HashMap::new();
    for (y, line) in input.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            map.insert(Position { x: x as i32, y: y as i32 }, c.to_digit(10).unwrap() as usize);
        }
    }
    map
}

fn a_star(map: HashMap<Position, usize>, min_movement: usize, max_movement: usize) -> usize {
    let target = *map.keys().max_by_key(|p| p.x * 1000 + p.y).unwrap();
    let start = Position {x: 0, y: 0};

    let mut queue = vec![Vec::new(); 256];
    let mut visited: HashMap<Position, Vec<usize>> = HashMap::new();

    let mut index = 0;
    queue[index].push((start, Direction::Down));
    queue[index].push((start, Direction::Right));

    loop {
        while let Some((pos, dir)) = queue[index % 256].pop() {
            let cost = visited.entry(pos).or_insert(vec![0; 4])[dir.index()];

            if pos == target {
                return cost;
            }

            let mut next_dirs: Vec<Direction> = Vec::new();
            if dir == Direction::Down || dir == Direction::Up {
                next_dirs.push(Direction::Left);
                next_dirs.push(Direction::Right);
            }
            else {
                next_dirs.push(Direction::Up);
                next_dirs.push(Direction::Down);
            }
            for next_dir in next_dirs {
                let mut next_cost = cost;
                let mut is_outside = false;
                for i in 1..min_movement {
                    let next = pos.next(i as i32, next_dir);
                    let opt_map = map.get(&next);
                    if opt_map.is_none() {
                        is_outside = true;
                    }
                    else {
                        next_cost += opt_map.unwrap();
                    }
                }

                if is_outside {
                    continue;
                }
                for i in min_movement..=max_movement { // add all 3 in same dir at once
                    let next = pos.next(i as i32, next_dir);
                    let opt_map = map.get(&next);
                    if opt_map.is_some() {
                        next_cost += opt_map.unwrap();

                        let old_cost = visited.entry(next).or_insert(vec![0; 4])[next_dir.index()];
                        if old_cost == 0 || next_cost < old_cost {
                            let heuristic = next_cost + next.manhattan(target);
                            queue[heuristic % 256].push((next, next_dir));
                            visited.entry(next).and_modify(|v| v[next_dir.index()] = next_cost);
                        }
                    }
                }
            }
        }
        index += 1;
    }

}
#[aoc(day17, part2)]
pub fn part2(input: &str) -> usize {
    let map = gen_map(input);

    a_star(map, 4, 10)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1("2413432311323
3215453535623
3255245654254
3446585845452
4546657867536
1438598798454
4457876987766
3637877979653
4654967986887
4564679986453
1224686865563
2546548887735
4322674655533"), 102)
    }

    #[test]
    fn sample2() {
        assert_eq!(part2("2413432311323
3215453535623
3255245654254
3446585845452
4546657867536
1438598798454
4457876987766
3637877979653
4654967986887
4564679986453
1224686865563
2546548887735
4322674655533"), 94)
    }

    #[test]
    fn sample3() {
        assert_eq!(part2("111111111111
999999999991
999999999991
999999999991
999999999991"), 71)
    }
}
