use std::collections::HashMap;
use std::ops::Range;
use regex::Regex;

type Almanach = (Vec<i64>, SingleAlmanach, SingleAlmanach, SingleAlmanach, SingleAlmanach, SingleAlmanach, SingleAlmanach, SingleAlmanach);
type SingleAlmanach = HashMap<Range<i64>, i64>;

#[aoc_generator(day5)]
pub fn almanach_parser(input: &str) -> Almanach {
    let maps: Vec<&str> = input.split("\n\n").collect();

    let seeds: Vec<i64> = maps[0].split(' ').collect::<Vec<&str>>().iter().filter_map(|n| n.parse::<i64>().ok()).collect();
    let seed_2_soil = parse_map(maps[1]);
    let soil_2_fertilizer = parse_map(maps[2]);
    let fertilizer_2_water = parse_map(maps[3]);
    let water_2_light = parse_map(maps[4]);
    let light_2_temperature = parse_map(maps[5]);
    let temperature_2_humidity = parse_map(maps[6]);
    let humidity_2_location = parse_map(maps[7]);

    (seeds, seed_2_soil, soil_2_fertilizer, fertilizer_2_water, water_2_light, light_2_temperature, temperature_2_humidity, humidity_2_location)
}

#[aoc(day5, part1)]
pub fn part1(input: &Almanach) -> i64 {
    let mut locations: Vec<i64> = Vec::new();
    for seed in input.0.iter() {
        let mut next_num = lookup_next(&input.1, *seed);
        next_num = lookup_next(&input.2, next_num);
        next_num = lookup_next(&input.3, next_num);
        next_num = lookup_next(&input.4, next_num);
        next_num = lookup_next(&input.5, next_num);
        next_num = lookup_next(&input.6, next_num);
        next_num = lookup_next(&input.7, next_num);

        locations.push(next_num)
    }

    *locations.iter().min().unwrap()
}

fn lookup_next(almanach_map: &SingleAlmanach, num: i64) -> i64 {
    almanach_map.iter().filter(|(k, _)| k.contains(&num)).next().unwrap_or((&(0..0), &0)).1 + num
}

fn parse_map(input: &str) -> HashMap<Range<i64>, i64> {
    let mut result: HashMap<Range<i64>, i64> = HashMap::new();
    let regex = Regex::new(r"(\d*) (\d*) (\d*)").unwrap();
    for line in input.lines().skip(1) {
        let Some((_, [target, start, range])) = regex.captures(line).map(|c| c.extract()) else { unreachable!() };

        let start_num = start.parse::<i64>().unwrap();
        let end = start_num + range.parse::<i64>().unwrap();
        let offset = target.parse::<i64>().unwrap() - start_num;
        result.insert(start_num..end, offset);
    }

    result
}

#[aoc(day5, part2)]
pub fn part2(input: &Almanach) -> i64 {
    let seed_ranges: Vec<Range<i64>> = input.0.as_slice()
        .chunks(2)
        .map(|chunk| chunk[0]..chunk[0] + chunk[1])
        .collect();

    let mut next_num = lookup_next_ranges(&input.1, seed_ranges);
    next_num = lookup_next_ranges(&input.2, next_num);
    next_num = lookup_next_ranges(&input.3, next_num);
    next_num = lookup_next_ranges(&input.4, next_num);
    next_num = lookup_next_ranges(&input.5, next_num);
    next_num = lookup_next_ranges(&input.6, next_num);
    next_num = lookup_next_ranges(&input.7, next_num);

    *next_num.iter().map(|r| r.start).collect::<Vec<i64>>().iter().min().unwrap()
}

fn lookup_next_ranges(almanach_map: &SingleAlmanach, ranges: Vec<Range<i64>>) -> Vec<Range<i64>> {
    let mut result: Vec<Range<i64>> = Vec::new();
    for range in ranges {
        let mut current_range = range.clone();
        for (target_range, offset) in almanach_map {
            if current_range.start <= current_range.end && current_range.start < target_range.end && current_range.end > target_range.start {
                // before mapping
                if current_range.start < target_range.start {
                    result.push(current_range.start..target_range.start);
                    current_range.start = target_range.start;

                    // ends in mapping
                    if current_range.end < target_range.end {
                        result.push(current_range.start+offset..current_range.end+offset);
                        current_range.start = current_range.end + 1 // processed all, make range invalid
                    } else {
                        result.push(current_range.start+offset..target_range.end+offset);
                        current_range.start = target_range.end;
                    }
                }
                // ends in mapping
                else if current_range.end < target_range.end {
                    result.push(current_range.start+offset..current_range.end+offset);
                    current_range.start = current_range.end + 1 // processed all, make range invalid
                }
                // intersects
                else {
                    result.push(current_range.start+offset..target_range.end+offset);
                    current_range.start = target_range.end;
                }
            }
        }

        if current_range.start <= current_range.end {
            result.push(current_range);
        }
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1(&almanach_parser("seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4")), 35)
    }


    #[test]
    fn sample2() {
        assert_eq!(part2(&almanach_parser("seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4")), 46)
    }
}