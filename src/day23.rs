use std::collections::HashMap;
use itertools::Itertools;

type Position = (i32, i32);

#[derive(Copy, Clone, Eq, PartialEq)]
enum Direction {
    North, East, South, West
}

pub struct Map {
    map: HashMap<Position, char>,
    start: Position,
    end: Position
}

#[aoc_generator(day23)]
fn gen_map(input: &str) -> Map {
    let mut map = HashMap::new();
    let mut max_y = 0;
    for (y, line) in input.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            map.insert((x as i32, y as i32), c);
        }
        max_y = y;
    }

    let start = *map.iter().find(|e| e.0.1 == 0 && *e.1 == '.').unwrap().0;
    let end = *map.iter().find(|e| e.0.1 == max_y as i32 && *e.1 == '.').unwrap().0;

    Map {
        map,
        start,
        end
    }
}

#[aoc(day23, part1)]
pub fn part1(input: &Map) -> usize {
    find_longest_path(input.start, input.end, &input.map, '#', true)
}

// runs for several minutes, to optimize the map could be optimized by only using waypoints and assign lengths as weights
#[aoc(day23, part2)]
pub fn part2(input: &Map) -> usize {
    find_longest_path(input.start, input.end, &input.map, '#', false)
}

struct Stack {
    current: Position,
    visited: Vec<Position>,
    last_slope: char
}

fn find_longest_path(start: Position, end: Position, map: &HashMap<Position, char>, last_slope: char, handle_slopes: bool) -> usize {
    let mut queue = vec![Stack {
        current: start,
        visited: Vec::new(),
        last_slope
    }];

    let mut path_length = 0;
    while let Some(item) = queue.pop() {
        if item.current == end {
            path_length = path_length.max(item.visited.len());
        }
        let mut visited_with_current = item.visited.clone();
        visited_with_current.push(item.current);

        let tile = *map.get(&item.current).unwrap();
        let mut slope = item.last_slope;
        if tile != '.' {
            slope = tile;
        }
        for neighbour in get_allowed_neighbours(item.current, map, slope, visited_with_current.clone(), handle_slopes) {
            queue.push(Stack {
                current: neighbour,
                visited: visited_with_current.clone(),
                last_slope: slope
            });
        }
    }

    path_length
}

fn get_allowed_neighbours(pos: Position, map: &HashMap<Position, char>, last_slope: char, visited: Vec<Position>, handle_slopes: bool) -> Vec<Position> {
    let neighbours = vec![((pos.0 - 1, pos.1), Direction::West), ((pos.0 + 1, pos.1), Direction::East), ((pos.0, pos.1 - 1), Direction::North), ((pos.0, pos.1 + 1), Direction::South)];

    let filtered = neighbours.iter().filter_map(|(p, dir)| if !visited.contains(p) && is_allowed_tile(p, dir, map, last_slope, handle_slopes) { Some(*p) } else { None }).collect_vec();

    filtered
}

fn is_allowed_tile(pos: &Position, dir: &Direction, map: &HashMap<Position, char>, last_slope: char, handle_slopes: bool) -> bool {
    let tile = map.get(pos);
    if tile.is_some_and(|&c| c != '#' && (!handle_slopes || is_same_direction(*dir, c, last_slope)) ) {
        return true;
    }

    false
}

fn is_same_direction(dir: Direction, tile: char, last_slope: char) -> bool {
    match tile {
        '.' => true,
        '>' => last_slope == '#' ||
            last_slope == '>' ||
            (dir != Direction::West && last_slope == 'v') ||
            (dir != Direction::West && last_slope == '^'),
        '<' => last_slope == '#' ||
            last_slope == '<' ||
            (dir != Direction::East && last_slope == 'v') ||
            (dir != Direction::East && last_slope == '^'),
        'v' => last_slope == '#' ||
            last_slope == 'v' ||
            (dir != Direction::North && last_slope == '>') ||
            (dir != Direction::North && last_slope == '<'),
        '^' => last_slope == '#' ||
            last_slope == '^' ||
            (dir != Direction::South && last_slope == '>') ||
            (dir != Direction::South && last_slope == '<'),
        _ => unreachable!()
    }
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1(&gen_map("#.#####################
#.......#########...###
#######.#########.#.###
###.....#.>.>.###.#.###
###v#####.#v#.###.#.###
###.>...#.#.#.....#...#
###v###.#.#.#########.#
###...#.#.#.......#...#
#####.#.#.#######.#.###
#.....#.#.#.......#...#
#.#####.#.#.#########v#
#.#...#...#...###...>.#
#.#.#v#######v###.###v#
#...#.>.#...>.>.#.###.#
#####v#.#.###v#.#.###.#
#.....#...#...#.#.#...#
#.#########.###.#.#.###
#...###...#...#...#.###
###.###.#.###v#####v###
#...#...#.#.>.>.#.>.###
#.###.###.#.###.#.#v###
#.....###...###...#...#
#####################.#")), 94)
    }

    #[test]
    fn sample2() {
        assert_eq!(part2(&gen_map("#.#####################
#.......#########...###
#######.#########.#.###
###.....#.>.>.###.#.###
###v#####.#v#.###.#.###
###.>...#.#.#.....#...#
###v###.#.#.#########.#
###...#.#.#.......#...#
#####.#.#.#######.#.###
#.....#.#.#.......#...#
#.#####.#.#.#########v#
#.#...#...#...###...>.#
#.#.#v#######v###.###v#
#...#.>.#...>.>.#.###.#
#####v#.#.###v#.#.###.#
#.....#...#...#.#.#...#
#.#########.###.#.#.###
#...###...#...#...#.###
###.###.#.###v#####v###
#...#...#.#.>.>.#.>.###
#.###.###.#.###.#.#v###
#.....###...###...#...#
#####################.#")), 154)
    }
}
