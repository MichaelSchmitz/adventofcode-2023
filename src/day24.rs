use std::ops::Range;
use itertools::Itertools;
use regex::Regex;
use z3::{ast, Config, Context, Solver};
use z3::ast::Ast;

#[derive(Debug)]
pub struct Hailstone {
    x: f64,
    y: f64,
    z: f64,
    v_x: f64,
    v_y: f64,
    v_z: f64
}

impl Hailstone {
    fn intersect_2d(&self, other: &Hailstone) -> Option<(f64, f64)> {
        let c1 = self.v_y * self.x + (-self.v_x) * self.y;
        let c2 = other.v_y * other.x + (-other.v_x) * other.y;
        let determinant = self.v_y*(-other.v_x) - other.v_y*(-self.v_x);
        if determinant == 0.0 {
            None
        }
        else {
            Some((((-other.v_x)*c1 - (-self.v_x)*c2) / determinant, (self.v_y*c2 - other.v_y*c1) / determinant))
        }
    }

    fn is_not_past(&self, x: f64, y: f64) -> bool {
        let x_allowed = if self.v_x > 0.0 {
            self.x < x
        }
        else {
            self.x > x
        };
        let y_allowed = if self.v_y > 0.0 {
            self.y < y
        }
        else {
            self.y > y
        };
        x_allowed && y_allowed
    }
}

fn find_all_intersects_in_area(input: &Vec<Hailstone>, area: Range<f64>) -> usize {
    let mut count = 0;
    for (a, b) in input.iter().tuple_combinations() {
        let p = a.intersect_2d(b);
        if p.is_none() {
            continue;
        }
        let (p_x, p_y) = p.unwrap();
        if area.contains(&p_x) && area.contains(&p_y) && a.is_not_past(p_x, p_y) && b.is_not_past(p_x, p_y)
        {
            count += 1;
        }
    }

    count
}

#[aoc_generator(day24)]
pub fn gen_hailstones(input: &str) -> Vec<Hailstone> {
    let regex = Regex::new(r"([-\d]*), *([-\d]*), *([-\d]*) @ *([-\d]*), *([-\d]*), *([-\d]*)").unwrap();
    let mut hailstones = Vec::new();
    for line in input.lines() {
        let Some((_, [x, y, z, v_x, v_y, v_z])) = regex.captures(line).map(|c| c.extract()) else { unreachable!() };
        hailstones.push(Hailstone {
            x: x.parse::<f64>().unwrap(),
            y: y.parse::<f64>().unwrap(),
            z: z.parse::<f64>().unwrap(),
            v_x: v_x.parse::<f64>().unwrap(),
            v_y: v_y.parse::<f64>().unwrap(),
            v_z: v_z.parse::<f64>().unwrap()
        })
    }

    hailstones
}

#[aoc(day24, part1)]
pub fn part1(input: &Vec<Hailstone>) -> usize {
    find_all_intersects_in_area(input, 200_000_000_000_000f64..400_000_000_000_001f64)
}

#[aoc(day24, part2)]
pub fn part2(input: &Vec<Hailstone>) -> u64 {
    let cfg = Config::new();
    let ctx = Context::new(&cfg);
    let solver = Solver::new(&ctx);

    let x = ast::Int::new_const(&ctx, "x");
    let y = ast::Int::new_const(&ctx, "y");
    let z = ast::Int::new_const(&ctx, "z");
    let v_x = ast::Int::new_const(&ctx, "v_x");
    let v_y = ast::Int::new_const(&ctx, "v_y");
    let v_z = ast::Int::new_const(&ctx, "v_z");

    for (i, hailstone) in input.iter().enumerate() {
        let t_i = ast::Int::new_const(&ctx, format!("t_{}", i));
        solver.assert(&(x.clone() + v_x.clone() * t_i.clone())._eq(&(ast::Int::from_i64(&ctx, hailstone.x as i64) + ast::Int::from_i64(&ctx, hailstone.v_x as i64) * t_i.clone())));
        solver.assert(&(y.clone() + v_y.clone() * t_i.clone())._eq(&(ast::Int::from_i64(&ctx, hailstone.y as i64) + ast::Int::from_i64(&ctx, hailstone.v_y as i64) * t_i.clone())));
        solver.assert(&(z.clone() + v_z.clone() * t_i.clone())._eq(&(ast::Int::from_i64(&ctx, hailstone.z as i64) + ast::Int::from_i64(&ctx, hailstone.v_z as i64) * t_i.clone())));
    }

    if solver.check() == z3::SatResult::Sat {
        let m = solver.get_model().unwrap();
        m.eval(&(x + y + z), true).unwrap().as_u64().unwrap()
    }
    else  {
        0
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(find_all_intersects_in_area(&gen_hailstones("19, 13, 30 @ -2,  1, -2
18, 19, 22 @ -1, -1, -2
20, 25, 34 @ -2, -2, -4
12, 31, 28 @ -1, -2, -1
20, 19, 15 @  1, -5, -3"), 7f64..28f64), 2)
    }

    #[test]
    fn sample2() {
        assert_eq!(part2(&gen_hailstones("19, 13, 30 @ -2,  1, -2
18, 19, 22 @ -1, -1, -2
20, 25, 34 @ -2, -2, -4
12, 31, 28 @ -1, -2, -1
20, 19, 15 @  1, -5, -3")), 47)
    }
}
