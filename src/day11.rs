use itertools::Itertools;

#[aoc(day11, part1)]
pub fn part1(input: &str) -> usize {
    let map = input.lines().map(|line| line.chars().collect_vec()).collect_vec();
    let galaxies = expand_map(map,
                              2);

    let mut paths = 0;
    for (a, b) in galaxies.iter().tuple_combinations() {
        paths += calc_dist(*a, *b);
    }

    paths
}

fn expand_map(map: Vec<Vec<char>>, expansion_size: usize) -> Vec<(usize, usize)> {
    let mut galaxies: Vec<(usize, usize)> = Vec::new();
    let mut empty_rows: Vec<usize> = Vec::new();

    for (y, row) in map.iter().enumerate() {
        if !row.contains(&'#') {
            empty_rows.push(y);
        }
        for (x, c) in row.iter().enumerate() {
            if *c == '#' {
                galaxies.push((x, y));
            }
        }
    }
    let mut empty_columns: Vec<usize> = Vec::new();
    for x in 0..map[0].len() {
        if map.iter().all(|vec| vec[x] == '.') {
            empty_columns.push(x);
        }
    }

    let mut expanded_galaxies: Vec<(usize, usize)> = Vec::new();
    for galaxy in galaxies {
        let mut x_offset = empty_columns.iter().filter(|&x| *x < galaxy.0).count();
        x_offset *= expansion_size - 1;
        let mut y_offset = empty_rows.iter().filter(|&y| *y < galaxy.1).count();
        y_offset *= expansion_size - 1;
        expanded_galaxies.push((galaxy.0 + x_offset, galaxy.1 + y_offset));
    }

    expanded_galaxies
}

fn calc_dist(a: (usize, usize), b: (usize, usize)) -> usize {
    usize::abs_diff(a.0, b.0) + usize::abs_diff(a.1, b.1)
}

#[aoc(day11, part2)]
pub fn part2(input: &str) -> usize {
    part2_sub(input, 1_000_000)
}


pub fn part2_sub(input: &str, expansion_size: usize) -> usize {
    let map = input.lines().map(|line| line.chars().collect_vec()).collect_vec();
    let galaxies = expand_map(map, expansion_size);

    let mut paths = 0;
    for (a, b) in galaxies.iter().tuple_combinations() {
        paths += calc_dist(*a, *b);
    }

    paths
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1("...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#....."), 374)
    }

    #[test]
    fn sample2() {
        assert_eq!(part2_sub("...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....", 10), 1030)
    }

    #[test]
    fn sample3() {
        assert_eq!(part2_sub("...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....", 100), 8410)
    }
}
