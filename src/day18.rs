use itertools::Itertools;

#[aoc(day18, part1)]
pub fn part1(input: &str) -> i64 {
    let mut x = 0;
    let mut y = 0;
    let mut points: Vec<(i64, i64)> = Vec::new();
    for line in input.lines() {
        let instructions = line.split(' ').collect_vec();
        let amount = instructions[1].parse::<i64>().unwrap();
        match instructions[0] {
            "R" => x += amount,
            "D" => y += amount,
            "L" => x -= amount,
            "U" => y -= amount,
            _ => unreachable!()
        }
        points.push((x, y));
    }
    calc_polygon_area_with_perimeter(points)
}

#[aoc(day18, part2)]
pub fn part2(input: &str) -> i64 {
    let mut x = 0;
    let mut y = 0;
    let mut points: Vec<(i64, i64)> = Vec::new();
    for line in input.lines() {
        let instructions = line.split(' ').collect_vec();
        let amount = i64::from_str_radix(&instructions[2][2..7], 16).unwrap();
        match instructions[2].as_bytes()[7] {
            b'0' => x += amount,
            b'1' => y += amount,
            b'2' => x -= amount,
            b'3' => y -= amount,
            _ => unreachable!()
        }
        points.push((x, y));
    }
    calc_polygon_area_with_perimeter(points)
}

fn calc_polygon_area_with_perimeter(points: Vec<(i64, i64)>) -> i64 {
    points.iter().circular_tuple_windows().map(|(a, b)| a.0 * b.1 - b.0 * a.1 + (a.0 - b.0).abs() + (a.1 - b.1).abs()).sum::<i64>().abs() / 2 + 1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1("R 6 (#70c710)
D 5 (#0dc571)
L 2 (#5713f0)
D 2 (#d2c081)
R 2 (#59c680)
D 2 (#411b91)
L 5 (#8ceee2)
U 2 (#caa173)
L 1 (#1b58a2)
U 2 (#caa171)
R 2 (#7807d2)
U 3 (#a77fa3)
L 2 (#015232)
U 2 (#7a21e3)"), 62)
    }

    #[test]
    fn sample2() {
        assert_eq!(part2("R 6 (#70c710)
D 5 (#0dc571)
L 2 (#5713f0)
D 2 (#d2c081)
R 2 (#59c680)
D 2 (#411b91)
L 5 (#8ceee2)
U 2 (#caa173)
L 1 (#1b58a2)
U 2 (#caa171)
R 2 (#7807d2)
U 3 (#a77fa3)
L 2 (#015232)
U 2 (#7a21e3)"), 952408144115)
    }
}
