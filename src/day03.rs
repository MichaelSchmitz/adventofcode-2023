use std::collections::HashMap;

type Position = (usize, usize);
const DEBUG: bool = false;
const EMPTY_LINE: &str = "............................................................................................................................................";
#[aoc(day3, part1)]
pub fn part1(input: &str) -> u32 {
    let mut part_sum = 0;
    let max_y = input.lines().collect::<Vec<&str>>().len() - 1;

    for (y, line) in input.lines().enumerate() {
        let last_line: Vec<char> = if y == 0 { EMPTY_LINE } else { input.lines().collect::<Vec<&str>>()[y - 1] }.chars().collect();
        let next_line: Vec<char> = if y == max_y { EMPTY_LINE } else { input.lines().collect::<Vec<&str>>()[y + 1] }.chars().collect();
        let max_x = line.trim().chars().collect::<Vec<char>>().len() - 1;
        let mut num = 0;
        let mut has_symbol = false;
        let mut last_char = '.';
        for (x, c) in line.trim().chars().enumerate() {
            if c.is_numeric() {
                // first digit
                if num == 0 {
                    if is_symbol(last_char) || // left of num
                        (x > 0 && (is_symbol(last_line[x-1]) || // left above num
                            is_symbol(next_line[x-1])))  { // left below num
                        has_symbol = true;
                    }
                }
                num = num * 10 + c.to_digit(10).unwrap();
                if is_symbol(last_line[x]) || // above digit
                    is_symbol(next_line[x]) { // below digit
                    has_symbol = true;
                }
            }
            else if num != 0 && (is_symbol(c) || // right of num
                has_symbol || // any other
                (x <= max_x && (
                    is_symbol(last_line[x]) || // right above num
                    is_symbol(next_line[x]))) // right below num
            ) {
                if DEBUG {
                    println!("Found part {}", num);
                }
                part_sum += num;
                num = 0;
                has_symbol = false;
            }
            else {
                if num != 0 && DEBUG {
                    println!("No part: {}", num);
                }
                num = 0;
                has_symbol = false;
            }
            last_char = c;
        }
        if has_symbol && num != 0 {
            if DEBUG {
                println!("Found part {}", num);
            }
            part_sum += num;
        }
    }

    part_sum
}

fn is_symbol(c: char) -> bool {
    !c.is_numeric() && c != '.'
}
fn is_gear_candiate(c: char) -> bool {
    c == '*'
}

#[aoc(day3, part2)]
pub fn part2(input: &str) -> u32 {
    let mut gear_sum = 0;
    let max_y = input.lines().collect::<Vec<&str>>().len() - 1;
    let mut gear_candiates: HashMap<Position, u32> = HashMap::new();
    for (y, line) in input.lines().enumerate() {
        let last_line: Vec<char> = if y == 0 { EMPTY_LINE } else { input.lines().collect::<Vec<&str>>()[y - 1] }.chars().collect();
        let next_line: Vec<char> = if y == max_y { EMPTY_LINE } else { input.lines().collect::<Vec<&str>>()[y + 1] }.chars().collect();
        let max_x = line.trim().chars().collect::<Vec<char>>().len() - 1;
        let mut num = 0;
        let mut gear_candidate: Option<Position> = None;
        let mut last_char = '.';
        for (x, c) in line.trim().chars().enumerate() {
            if c.is_numeric() {
                // first digit
                if num == 0 {
                    if is_gear_candiate(last_char) {
                        gear_candidate = Some((x - 1, y));
                    } else if x > 0 {
                        if is_gear_candiate(last_line[x-1]) {
                            gear_candidate = Some((x - 1, y -1));
                        } else if is_gear_candiate(next_line[x-1]) {
                            gear_candidate = Some((x - 1, y + 1));
                        }
                    }
                }
                num = num * 10 + c.to_digit(10).unwrap();
                if is_gear_candiate(last_line[x]) {
                    gear_candidate = Some((x, y - 1));
                } else if is_gear_candiate(next_line[x]) {
                    gear_candidate = Some((x, y + 1));
                }
            }
            else if num != 0 {
                if is_gear_candiate(c) {
                    gear_candidate = Some((x, y));
                }
                else if x <= max_x {
                    if is_gear_candiate(last_line[x]) {
                        gear_candidate = Some((x, y - 1));
                    }
                    else if is_gear_candiate(next_line[x]) {
                        gear_candidate = Some((x, y + 1));
                    }
                }

                if gear_candidate.is_some() {
                    let coord = gear_candidate.unwrap();
                    let old_candidate = gear_candiates.get(&coord);
                    if old_candidate.is_some() {
                        let ratio = old_candidate.unwrap() * num;
                        if DEBUG {
                            println!("Found gear at x: {}, y: {} with ratio {}", coord.0, coord.1, ratio);
                        }
                        gear_sum += ratio;
                    }
                    else {
                        if DEBUG {
                            println!("Found gear candidate at x: {}, y: {} with ratio factor {}", coord.0, coord.1, num);
                        }
                        gear_candiates.insert(coord, num);
                    }
                }
                else if DEBUG {
                    println!("No gear candidate: {}", num);
                }
                num = 0;
                gear_candidate = None;
            }
            last_char = c;
        }
        if gear_candidate.is_some() && num != 0 {
            let coord = gear_candidate.unwrap();
            let old_candidate = gear_candiates.get(&coord);
            if old_candidate.is_some() {
                let ratio = old_candidate.unwrap() * num;
                if DEBUG {
                    println!("Found gear at x: {}, y: {} with ratio {}", coord.0, coord.1, ratio);
                }
                gear_sum += ratio;
            }
        }
    }

    gear_sum
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1("467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598.."), 4361)
    }
    #[test]
    fn sample2() {
        assert_eq!(part1("1*.*2..*..
.3..4.5...
6.7.....*.
.*....8..9"), 37)
    }
    #[test]
    fn sample3() {
        assert_eq!(part2("467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598.."), 467835)
    }
}