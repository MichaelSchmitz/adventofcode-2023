use std::collections::HashMap;
use std::ops::Range;
use itertools::Itertools;
use regex::Regex;

#[derive(Copy, Clone)]
struct Part {
    x: u32,
    m: u32,
    a: u32,
    s: u32,
}

#[derive(Debug)]
struct RangePart {
    x: Range<u32>,
    m: Range<u32>,
    a: Range<u32>,
    s: Range<u32>,
}

impl RangePart {
    fn is_valid(&self) -> bool {
        self.x.start < self.x.end &&
            self.m.start < self.m.end &&
            self.a.start < self.a.end &&
            self.s.start < self.s.end
    }

    fn count(&self) -> usize {
        self.x.len() * self.a.len()  * self.m.len() * self.s.len()
    }
}

struct Rule {
    category: Option<char>,
    target: String,
    compare: u32,
    op: char,
}

impl Rule {
    fn is_rule_match(&self, p: Part) -> bool {
        self.category.is_none() || self.category.is_some_and(|c| match c {
            'x' => self.apply(p.x),
            'm' => self.apply(p.m),
            'a' => self.apply(p.a),
            's' => self.apply(p.s),
            _ => unreachable!()
        })
    }

    fn apply_rule_range(&self, p: RangePart) -> (RangePart, RangePart) {
        if self.category.is_none() {
            return (p, RangePart {
                x: 1..0,
                m: 1..0,
                a: 1..0,
                s: 1..0,
            })
        }
        let cat = self.category.unwrap();
        if cat == 'x' {
            let r = self.apply_to_range(p.x);
            return (RangePart {
                x: r.0,
                m: p.m.clone(),
                a: p.a.clone(),
                s: p.s.clone()
            },RangePart {
                x: r.1,
                m: p.m,
                a: p.a,
                s: p.s
            })
        }
        else if cat == 'm' {
            let r = self.apply_to_range(p.m);
            return (RangePart {
                x: p.x.clone(),
                m: r.0,
                a: p.a.clone(),
                s: p.s.clone()
            },RangePart {
                x: p.x,
                m: r.1,
                a: p.a,
                s: p.s
            })
        }
        else if cat == 'a' {
            let r = self.apply_to_range(p.a);
            return (RangePart {
                x: p.x.clone(),
                m: p.m.clone(),
                a: r.0,
                s: p.s.clone()
            },RangePart {
                x: p.x,
                m: p.m,
                a: r.1,
                s: p.s
            })
        }
        else if cat == 's' {
            let r = self.apply_to_range(p.s);
            return (RangePart {
                x: p.x.clone(),
                m: p.m.clone(),
                a: p.a.clone(),
                s: r.0
            },RangePart {
                x: p.x,
                m: p.m,
                a: p.a,
                s: r.1
            })
        }

        unreachable!()
    }

    fn apply_to_range(&self, range: Range<u32>) -> (Range<u32>, Range<u32>) {
        match self.op {
            '<' => (range.start..self.compare.min(range.end), self.compare.max(range.start)..range.end),
            '>' => (self.compare.max(range.start)+1..range.end, range.start..self.compare.min(range.end)+1),
            _ => unreachable!()
        }
    }

    fn apply(&self, val: u32) -> bool {
        match self.op {
            '<' => val < self.compare,
            '>' => val > self.compare,
            _ => unreachable!()
        }
    }
}

fn parse_rule(rule: &str) -> Rule {
    if rule.contains(':') {
        let (left, target) = rule.split_once(':').unwrap();
        let op = if left.contains('<') { '<' } else { '>' };
        let (category, num) = left.split_once(op).unwrap();
        Rule {
            category: Some(category.chars().collect_vec()[0]),
            target: target.trim_end_matches(|p| p == '}').to_string(),
            compare: num.parse::<u32>().unwrap(),
            op,
        }
    } else {
        Rule {
            category: None,
            target: rule.trim_end_matches(|p| p == '}').to_string(),
            compare: 0,
            op: '<',
        }
    }
}

#[aoc(day19, part1)]
pub fn part1(input: &str) -> u32 {
    let (workflows, part_input) = extract_workflows(input);

    let mut sum = 0;
    let regex = Regex::new(r".*=(\d+).*=(\d+).*=(\d+).*?=(\d*).*").unwrap();
    for part in part_input.lines() {
        let Some((_, [x, m, a, s])) = regex.captures(part).map(|c| c.extract()) else { unreachable!() };
        let p = Part { x: x.parse::<u32>().unwrap(), m: m.parse::<u32>().unwrap(), a: a.parse::<u32>().unwrap(), s: s.parse::<u32>().unwrap() };

        if process_part(p, &workflows) {
            sum += p.x + p.m + p.a + p.s
        }
    }
    sum
}

fn extract_workflows(input: &str) -> (HashMap<&str, Vec<Rule>>, &str) {
    let mut workflows: HashMap<&str, Vec<Rule>> = HashMap::new();
    let (workflow_input, part_input) = input.split_once("\n\n").unwrap();

    for workflow in workflow_input.lines() {
        let (name, rules) = workflow.split_once('{').unwrap();
        let rules_parsed = rules.split(',').map(|x| parse_rule(x)).collect_vec();
        workflows.insert(name, rules_parsed);
    }
    (workflows, part_input)
}

fn process_part(p: Part, workflows: &HashMap<&str, Vec<Rule>>) -> bool {
    let mut workflow = "in".to_string();
    loop {
        workflow = process_rules(p, workflows, workflow.as_str());
        if workflow == "A" {
            return true;
        } else if workflow == "R" {
            return false;
        }
    }
}

fn process_rules(p: Part, workflows: &HashMap<&str, Vec<Rule>>, workflow: &str) -> String {
    let rules = workflows.get(workflow).unwrap();
    for rule in rules {
        if rule.is_rule_match(p) {
            return rule.target.clone();
        }
    }

    unreachable!()
}

#[aoc(day19, part2)]
pub fn part2(input: &str) -> usize {
    let (workflows, _) = extract_workflows(input);

    let p = RangePart {
        x: 1..4001,
        m: 1..4001,
        a: 1..4001,
        s: 1..4001,
    };

    count_possible(p, &workflows)
}

fn count_possible(p: RangePart, workflows: &HashMap<&str, Vec<Rule>>) -> usize {
    let mut possible = 0;
    let mut stack = vec![(p, "in".to_string())];
    while let Some((part, workflow)) = stack.pop() {
        for applied in process_rules_range(part, workflows, workflow.as_str()) {
            if applied.1 == "A" {
                possible += applied.0.count();
            } else if applied.1 != "R" {
                stack.push(applied);
            }
        }
    }
    possible
}

fn process_rules_range(p: RangePart, workflows: &HashMap<&str, Vec<Rule>>, workflow: &str) -> Vec<(RangePart, String)> {
    let rules = workflows.get(workflow).unwrap();
    let mut part = p;
    let mut targets: Vec<(RangePart, String)> = Vec::new();

    for rule in rules {
        let (correct, failed) = rule.apply_rule_range(part);
        if correct.is_valid() {
            targets.push((correct, rule.target.clone()));
        }
        if !failed.is_valid() {
            return targets;
        }
        part = failed;
    }

    unreachable!()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1("px{a<2006:qkq,m>2090:A,rfg}
pv{a>1716:R,A}
lnx{m>1548:A,A}
rfg{s<537:gd,x>2440:R,A}
qs{s>3448:A,lnx}
qkq{x<1416:A,crn}
crn{x>2662:A,R}
in{s<1351:px,qqz}
qqz{s>2770:qs,m<1801:hdj,R}
gd{a>3333:R,R}
hdj{m>838:A,pv}

{x=787,m=2655,a=1222,s=2876}
{x=1679,m=44,a=2067,s=496}
{x=2036,m=264,a=79,s=2244}
{x=2461,m=1339,a=466,s=291}
{x=2127,m=1623,a=2188,s=1013}"), 19114)
    }

    #[test]
    fn sample2() {
        assert_eq!(part2("px{a<2006:qkq,m>2090:A,rfg}
pv{a>1716:R,A}
lnx{m>1548:A,A}
rfg{s<537:gd,x>2440:R,A}
qs{s>3448:A,lnx}
qkq{x<1416:A,crn}
crn{x>2662:A,R}
in{s<1351:px,qqz}
qqz{s>2770:qs,m<1801:hdj,R}
gd{a>3333:R,R}
hdj{m>838:A,pv}

{x=787,m=2655,a=1222,s=2876}
{x=1679,m=44,a=2067,s=496}
{x=2036,m=264,a=79,s=2244}
{x=2461,m=1339,a=466,s=291}
{x=2127,m=1623,a=2188,s=1013}"), 167409079868000)
    }
}