use std::collections::HashMap;

#[aoc(day4, part1)]
pub fn part1(input: &str) -> u32 {
    let mut total_points = 0;
    for card in input.lines() {
        let mut card_points = 0;
        let split: Vec<&str> = card.split(':').collect();
        let nums: Vec<&str> = split[1].split('|').collect();
        let winning: Vec<u32> = nums[0].trim().split(' ').collect::<Vec<&str>>().iter().filter_map(|n| n.parse::<u32>().ok()).collect();
        let on_card: Vec<&str> = nums[1].trim().split(' ').collect();

        for num_on_card in on_card {
            if num_on_card.parse::<u32>().is_ok_and(|n| winning.contains(&n))
            {
                card_points = if card_points == 0 { 1 } else { card_points * 2 };
            }
        }
        total_points += card_points
    }

    total_points
}

#[aoc(day4, part2)]
pub fn part2(input: &str) -> u32 {
    let mut total_cards = 0;

    let (initial_cards, cards) = parse_cards(input);

    let mut card_cache: HashMap<u32, u32> = HashMap::new();

    for card in initial_cards {
        total_cards += calc_card(&card, &cards, &mut card_cache);
    }

    total_cards
}

fn calc_card(card: &u32, cards: &HashMap<u32, Vec<u32>>, card_cache: &mut HashMap<u32, u32>) -> u32 {
    let won_cards = cards.get(card);
    let mut result = 1;
    if won_cards.is_some_and(|v| v.iter().count() > 0) {
        for won_card in won_cards.unwrap() {
            let cached = card_cache.get(won_card);
            if cached.is_some() {
                result += cached.unwrap()
            }
            else {
                let res = calc_card(won_card, cards, card_cache);
                card_cache.insert(*won_card, res);
                result += res;
            }
        }
    }

    result
}

fn parse_cards(input: &str) -> (Vec<u32>, HashMap<u32, Vec<u32>>) {
    let mut cards: HashMap<u32, Vec<u32>> = HashMap::new();
    let mut initial_cards: Vec<u32> = Vec::new();

    for card in input.lines() {
        let split: Vec<&str> = card.split(':').collect();
        let card_num = split[0][4..].trim().parse::<u32>().unwrap();
        initial_cards.push(card_num);
        let nums: Vec<&str> = split[1].split('|').collect();
        let winning: Vec<u32> = nums[0].trim().split(' ').collect::<Vec<&str>>().iter().filter_map(|n| n.parse::<u32>().ok()).collect();
        let on_card: Vec<&str> = nums[1].trim().split(' ').collect();
        let mut card_wins = 0;
        for num_on_card in on_card {
            if num_on_card.parse::<u32>().is_ok_and(|n| winning.contains(&n))
            {
                card_wins += 1;
            }
        }
        cards.insert(card_num, (card_num+1..card_num+card_wins+1).collect());
    }

    (initial_cards, cards)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53"), 8)
    }
    #[test]
    fn sample2() {
        assert_eq!(part1("Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19"), 2)
    }
    #[test]
    fn sample3() {
        assert_eq!(part1("Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1"), 2)
    }
    #[test]
    fn sample4() {
        assert_eq!(part1("Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83"), 1)
    }
    #[test]
    fn sample5() {
        assert_eq!(part1("Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36"), 0)
    }
    #[test]
    fn sample6() {
        assert_eq!(part1("Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"), 0)
    }
    #[test]
    fn sample7() {
        assert_eq!(part1("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"), 13)
    }
    #[test]
    fn sample8() {
        assert_eq!(part2("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53"), 5)
    }
    #[test]
    fn sample9() {
        assert_eq!(part2("Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19"), 3)
    }
    #[test]
    fn sample10() {
        assert_eq!(part2("Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1"), 3)
    }
    #[test]
    fn sample11() {
        assert_eq!(part2("Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83"), 2)
    }
    #[test]
    fn sample12() {
        assert_eq!(part2("Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36"), 1)
    }
    #[test]
    fn sample13() {
        assert_eq!(part2("Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"), 1)
    }
    #[test]
    fn sample14() {
        assert_eq!(part2("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"), 30)
    }
}