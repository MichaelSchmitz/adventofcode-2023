use std::cmp::min;
use itertools::Itertools;

#[aoc(day13, part1)]
pub fn part1(input: &str) -> usize {
    let patterns = input.split("\n\n").collect_vec();

    let mut sum = 0;
    for pattern in patterns {
        let (v, h) = solve_pattern(pattern, None, None);
        sum += v * 100 + h
    }

    sum
}

fn solve_pattern(input: &str, ignore_v: Option<usize>, ignore_h: Option<usize>) -> (usize, usize) {
    let mut helpers: Vec<Vec<char>> = Vec::new();
    for line in input.lines() {
        if helpers.len() == line.len() {
            for (i, c) in line.chars().enumerate() {
                helpers[i].push(c);
            }
        }
        else {
            for c in line.chars() {
                helpers.push(vec![c])
            }
        }
    }
    let columns = helpers.iter().map(|chars| String::from_iter(chars)).collect_vec();

    (find_same(input.lines().map(|s| s.to_string()).collect_vec(), ignore_v), find_same(columns, ignore_h))
}

fn find_same(lines: Vec<String>, ignore: Option<usize>) -> usize {
    let matches = lines.iter().enumerate().tuple_windows().filter_map(|((a_i, a), (b_i, b))| if a == b { Some((a_i, b_i)) } else { None }).collect_vec();
    for (a, b) in matches {
        let items = min(a, lines.len() - b -1);
        let skip = if items < a { a - items } else { 0 };
        let left = lines.iter().skip(skip).take(items).collect_vec();
        let mut right = lines.iter().skip(b+1).take(items).collect_vec();
        right.reverse();
        if left.iter().zip(right).all(|(l, r)| *l == r)
        {
            if ignore.is_some_and(|i| a + 1 == i)
            {
                continue;
            }
            return a + 1;
        }
    }
    0
}

#[aoc(day13, part2)]
pub fn part2(input: &str) -> usize {
    let patterns = input.split("\n\n").collect_vec();

    let mut sum = 0;
    for pattern in patterns {
        sum += solve_with_replacement(pattern)
    }

    sum
}

fn solve_with_replacement(pattern: &str) -> usize {
    let original = solve_pattern(pattern, None, None);
    for i in 0..pattern.len() {
        let mut chars = pattern.chars().collect_vec();
        if chars[i] == '\n' {
            continue;
        }
        chars[i] = if chars[i] == '#' { '.' } else { '#' };
        let s = String::from_iter(chars);
        let mirror = solve_pattern(s.as_str(), Some(original.0), Some(original.1));
        if mirror == (0, 0)
        {
            continue;
        }
        if mirror != original {
                if mirror.0 == original.0 || mirror.0 == 0 {
                    return mirror.1
                } else if mirror.1 == original.1 || mirror.1 == 0 {
                    return mirror.0 * 100
                }
        }
    }

    println!("{}", pattern);

    unreachable!()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1("#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#."), 5)
    }
    #[test]
    fn sample2() {
        assert_eq!(part1("#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#"), 400)
    }

    #[test]
    fn sample3() {
        assert_eq!(part1("#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#"), 405)
    }
    #[test]
    fn sample4() {
        assert_eq!(part1(".#.###.###..###.#
..#...##########.
..######.#..#.###
..##.#.#......#..
..#.##....##....#
.##..#.##.##.##.#
#.....####..####.
#.....####..####.
.##..#.##.##.##.#"), 700)
    }

    #[test]
    fn sample5() {
        assert_eq!(part2("#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#."), 300)
    }
    #[test]
    fn sample6() {
        assert_eq!(part2("#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#"), 100)
    }

    #[test]
    fn sample7() {
        assert_eq!(part2("#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#"), 400)
    }
    #[test]
    fn sample8() {
        assert_eq!(part2(".#.###.###..###.#
..#...##########.
..######.#..#.###
..##.#.#......#..
..#.##....##....#
.##..#.##.##.##.#
#.....####..####.
#.....####..####.
.##..#.##.##.##.#"), 11)
    }
    #[test]
    fn sample9() {
        assert_eq!(part2(".#.##.#.##..###
...##...#######
#.####.#.#.###.
#..##..##..#...
###..###....###
.##..##..#.#...
.#....#..######
#..##..########
########.#..#.."), 14)
    }
}
