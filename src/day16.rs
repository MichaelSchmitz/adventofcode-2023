use std::collections::HashMap;
use itertools::Itertools;

#[derive(Eq, PartialEq, Copy, Clone)]
struct Position {
    x: i32,
    y: i32,
    dir: Direction
}

trait Incrementer {
    fn pass_through(&self) -> Position;
    fn turn_left(&self) -> Position;
    fn turn_right(&self) -> Position;
    fn new_pos(x: i32, y: i32, from: Direction) -> Position;
}

impl Incrementer for Position {
    fn pass_through(&self) -> Position {
        Position::new_pos(self.x, self.y, self.dir)
    }
    fn turn_left(&self) -> Position {
        match self.dir {
            Direction::Up => Position::new_pos(self.x, self.y, Direction::Left),
            Direction::Down => Position::new_pos(self.x, self.y, Direction::Right),
            Direction::Left => Position::new_pos(self.x, self.y, Direction::Down),
            Direction::Right => Position::new_pos(self.x, self.y, Direction::Up),
        }
    }
    fn turn_right(&self) -> Position {
        match self.dir {
            Direction::Up => Position::new_pos(self.x, self.y, Direction::Right),
            Direction::Down => Position::new_pos(self.x, self.y, Direction::Left),
            Direction::Left => Position::new_pos(self.x, self.y, Direction::Up),
            Direction::Right => Position::new_pos(self.x, self.y, Direction::Down),
        }
    }

    fn new_pos(x: i32, y: i32, from: Direction) -> Position {
        match from {
            Direction::Up => Position { x, y: y - 1, dir: Direction::Up },
            Direction::Down => Position { x, y: y + 1, dir: Direction::Down },
            Direction::Left => Position { x: x - 1, y, dir: Direction::Left },
            Direction::Right => Position { x: x + 1, y, dir: Direction::Right },
        }
    }
}

#[derive(Eq, PartialEq, Copy, Clone)]
enum Direction {
    Up, Down, Left, Right
}

#[aoc(day16, part1)]
pub fn part1(input: &[u8]) -> usize {
    let contraption = generate_contraption(input);

    solve_arrangement(&contraption, Position { x: -1, y: 0, dir: Direction::Right })
}

fn solve_arrangement(contraption_in: &HashMap<(i32, i32), u8>, start: Position) -> usize {
    let mut contraption = contraption_in.clone();
    contraption.insert((start.x, start.y), b'.');
    let mut visited: Vec<Position> = Vec::new();
    let mut to_visit: Vec<Position> = vec![start];
    loop {
        let opt_tile = to_visit.pop();
        if opt_tile.is_none() {
            return visited.iter().unique_by(|p| p.x * 1000 + p.y).count() - 1;
        }
        let tile = opt_tile.unwrap();
        if visited.contains(&tile) {
            continue;
        }
        let b = contraption.get(&(tile.x, tile.y));
        if b.is_none() {
            continue;
        }

        match b.unwrap() {
            b'.' => to_visit.push(tile.pass_through()),
            b'|' => {
                if tile.dir == Direction::Up || tile.dir == Direction::Down {
                    to_visit.push(tile.pass_through())
                } else {
                    to_visit.push(tile.turn_left());
                    to_visit.push(tile.turn_right());
                }
            },
            b'-' => {
                if tile.dir == Direction::Left || tile.dir == Direction::Right {
                    to_visit.push(tile.pass_through())
                } else {
                    to_visit.push(tile.turn_left());
                    to_visit.push(tile.turn_right());
                }
            }
            b'/' => {
                if tile.dir == Direction::Right || tile.dir == Direction::Left {
                    to_visit.push(tile.turn_left());
                } else {
                    to_visit.push(tile.turn_right());
                }
            }
            b'\\' => {
                if tile.dir == Direction::Right || tile.dir == Direction::Left {
                    to_visit.push(tile.turn_right());
                } else {
                    to_visit.push(tile.turn_left());
                }
            }
            _ => unreachable!()
        }
        visited.push(tile);
    }
}

fn generate_contraption(input: &[u8]) -> HashMap<(i32, i32), u8> {
    let mut contraption: HashMap<(i32, i32), u8> = HashMap::new();
    let mut y = 0;
    let mut x = 0;
    for i in 0..input.len() {
        let field = input[i];
        if field == b'\n' {
            y += 1;
            x = 0;
        } else {
            contraption.insert((x, y), input[i]);
            x += 1;
        }
    }
    contraption
}

#[aoc(day16, part2)]
pub fn part2(input: &[u8]) -> usize {
    let contraption = generate_contraption(input);

    let max_x = contraption.iter().max_by_key(|(p, _)| p.0).unwrap().0.0;
    let max_y = contraption.iter().max_by_key(|(p, _)| p.1).unwrap().0.1;
    let mut starts: Vec<Position> = Vec::new();
    for y in 0..=max_y {
        starts.push(Position { x: -1, y, dir: Direction::Right });
        starts.push(Position { x: max_x + 1, y, dir: Direction::Left });
    }
    for x in 0..=max_x {
        starts.push(Position { x, y: -1, dir: Direction::Down });
        starts.push(Position { x, y: max_y + 1, dir: Direction::Up });
    }

    starts.iter().map(|p| solve_arrangement(&contraption, *p)).max().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1(".|...\\....
|.-.\\.....
.....|-...
........|.
..........
.........\\
..../.\\\\..
.-.-/..|..
.|....-|.\\
..//.|....".as_bytes()), 46)
    }

    #[test]
    fn sample2() {
        assert_eq!(part2(".|...\\....
|.-.\\.....
.....|-...
........|.
..........
.........\\
..../.\\\\..
.-.-/..|..
.|....-|.\\
..//.|....".as_bytes()), 51)
    }
}
