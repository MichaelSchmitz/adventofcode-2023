use std::collections::{HashMap, VecDeque};

use itertools::Itertools;
use num_integer::lcm;


trait Module {
    fn receive(&mut self, on: bool, from: String) -> Option<bool>;
    fn targets(&self) -> Vec<String>;
}
struct Broadcaster {
    targets: Vec<String>
}
struct FlipFlop {
    targets: Vec<String>,
    state: bool
}
struct Conjunction {
    targets: Vec<String>,
    states: HashMap<String, bool>
}

impl Module for FlipFlop {
    fn receive(&mut self, on: bool, _: String) -> Option<bool> {
        if !on {
            self.state = !self.state;
            Some(self.state)
        }
        else {
            None
        }
    }

    fn targets(&self) -> Vec<String> {
        self.targets.clone()
    }
}
impl Module for Conjunction {
    fn receive(&mut self, on: bool, from: String) -> Option<bool> {
        self.states.entry(from).and_modify(|s| { *s = on } ).or_insert(on);
        Some(!self.states.iter().all(|(_, &s)| s))
    }

    fn targets(&self) -> Vec<String> {
        self.targets.clone()
    }
}
impl Module for Broadcaster {
    fn receive(&mut self, on: bool, _: String) -> Option<bool> {
        Some(on)
    }

    fn targets(&self) -> Vec<String> {
        self.targets.clone()
    }
}

#[aoc(day20, part1)]
pub fn part1(input: &str) -> usize {
    let mut modules = generate_modules(input);
    let mut high_pulses = 0;
    let mut low_pulses = 0;
    for _ in 0..1000 {
        low_pulses += 1; // button
        let mut signals: VecDeque<(String, bool, String)> = VecDeque::new();
        let broadcaster = modules.get("broadcaster").unwrap();
        for target in broadcaster.targets() {
            low_pulses += 1;
            signals.push_back((target, false, "broadcaster".to_string()));
        }

        while let Some((name, signal, from)) = signals.pop_front() {
            let opt_module = modules.get_mut(name.as_str());
            if opt_module.is_none() {
                continue;
            }
            let module = opt_module.unwrap();
            let opt_output = module.receive(signal, from);
            if opt_output.is_none() {
                continue;
            }
            let output = opt_output.unwrap();
            for target in module.targets() {
                signals.push_back((target, output, name.clone()))
            }
            if output {
                high_pulses += module.targets().len();
            }
            else {
                low_pulses += module.targets().len();
            }
        }
    }

    high_pulses * low_pulses
}

fn generate_modules(input: &str) -> HashMap<String, Box<dyn Module>> {
    let mut modules: HashMap<String, Box<dyn Module>> = HashMap::new();
    let mut inverted_list: HashMap<String, HashMap<String, bool>> = HashMap::new();
    for line in input.lines() {
        let (module, targets) = line.split_once(" -> ").unwrap();
        let target_vec = targets.split(", ").map(|s| s.to_string()).collect_vec();
        let mut c = module.chars();
        let first = c.next().unwrap();
        let name = if first == 'b' { "broadcaster".to_string() } else { c.as_str().to_string() };
        for target in target_vec {
            inverted_list.entry(target).or_insert(HashMap::new()).insert(name.clone(), false);
        }
    }

    for line in input.lines() {
        let (module, targets) = line.split_once(" -> ").unwrap();
        let target_vec = targets.split(", ").map(|s| s.to_string()).collect_vec();
        let mut c = module.chars();
        let first = c.next().unwrap();
        let name = c.as_str().to_string();
        match first {
            'b' => {
                modules.insert("broadcaster".to_string(), Box::new(Broadcaster {
                    targets: target_vec
                }));
            },
            '%' => {
                modules.insert(name, Box::new(FlipFlop {
                    targets: target_vec,
                    state: false
                }));
            },
            '&' => {
                modules.insert(name.clone(), Box::new(Conjunction {
                    targets: target_vec,
                    states: inverted_list.get(&name).unwrap().clone()
                }));
            },
            _ => unreachable!()
        };
    }
    modules
}

#[aoc(day20, part2)]
pub fn part2(input: &str) -> usize {
    let mut modules = generate_modules(input);
    // These all have to turn high in order to trigger the cs conjunction module to send a low rx pulse
    // Find individual lengths for these and then calculate the lcm
    let mut kh = 0;
    let mut lz = 0;
    let mut tg = 0;
    let mut hn = 0;
    let mut button_presses = 0;
    loop {
        if kh != 0 && lz != 0 && tg != 0 && hn != 0 {
            return lcm(kh, lcm(lz, lcm(tg, hn)));
        }
        button_presses += 1;
        let mut signals: VecDeque<(String, bool, String)> = VecDeque::new();
        let broadcaster = modules.get("broadcaster").unwrap();
        for target in broadcaster.targets() {
            signals.push_back((target, false, "broadcaster".to_string()));
        }

        while let Some((name, signal, from)) = signals.pop_front() {
            let opt_module = modules.get_mut(name.as_str());
            if opt_module.is_none() {
                continue;
            }
            let module = opt_module.unwrap();
            let opt_output = module.receive(signal, from);
            if opt_output.is_none() {
                continue;
            }
            let output = opt_output.unwrap();
            for target in module.targets() {
                if target == "kh" && !output {
                    kh = button_presses;
                }
                else if target == "lz" && !output {
                    lz = button_presses;
                }
                else if target == "tg" && !output {
                    tg = button_presses;
                }
                else if target == "hn" && !output {
                    hn = button_presses;
                }
                signals.push_back((target, output, name.clone()))
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1("broadcaster -> a, b, c
%a -> b
%b -> c
%c -> inv
&inv -> a"), 32_000_000)
    }

    #[test]
    fn sample2() {
        assert_eq!(part1("broadcaster -> a
%a -> inv, con
&inv -> b
%b -> con
&con -> output"), 11_687_500)
    }
}