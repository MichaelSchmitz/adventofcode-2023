use std::cmp::Ordering;
use std::collections::HashMap;

pub fn parse_hands(input: &str) -> Vec<(&str, usize)> {
    let mut hands: Vec<(&str, usize)> = Vec::new();
    for line in input.lines() {
        let hand_bid: Vec<&str> = line.split(' ').collect();
        hands.push((hand_bid[0], hand_bid[1].trim().parse::<usize>().unwrap()))
    }

    hands
}

#[aoc(day7, part1)]
pub fn part1(input: &str) -> usize {
    let mut hands = parse_hands(input);
    hands.sort_by(|(hand, _), (other, _)| sort(hand, other, Box::new(char_counts), Box::new(sort_char)));

    calc_result(hands)
}

#[aoc(day7, part2)]
pub fn part2(input: &str) -> usize {
    let mut hands = parse_hands(input);
    hands.sort_by(|(hand, _), (other, _)| sort(hand, other, Box::new(char_counts_with_joker), Box::new(sort_char_with_joker)));

    calc_result(hands)
}

fn calc_result(hands: Vec<(&str, usize)>) -> usize {
    hands.iter().enumerate().map(|(i, h)| (i + 1) * h.1).collect::<Vec<usize>>().iter().sum()
}

fn sort(a: &str, b: &str, count_parser: Box<dyn Fn(&str) -> HashMap<char, i32>>, char_sorter: Box<dyn Fn(Option<(char, char)>) -> Ordering>) -> Ordering {
    let count_a = count_parser(a);
    let count_b = count_parser(b);
    if count_a.len() == count_b.len() {
        if count_a.len() == 2 {
            if is_four_of_a_kind(count_a) {
                if is_four_of_a_kind(count_b) {
                    sort_by_chars(a, b, char_sorter)
                } else {
                    Ordering::Greater
                }
            } else {
                if is_four_of_a_kind(count_b) {
                    Ordering::Less
                } else {
                    sort_by_chars(a, b, char_sorter)
                }
            }
        } else if count_a.len() == 3 {
            if is_three_of_a_kind(count_a) {
                if is_three_of_a_kind(count_b) {
                    sort_by_chars(a, b, char_sorter)
                } else {
                    Ordering::Greater
                }
            } else {
                if is_three_of_a_kind(count_b) {
                    Ordering::Less
                } else {
                    sort_by_chars(a, b, char_sorter)
                }
            }
        } else {
            sort_by_chars(a, b, char_sorter)
        }
    } else {
        if count_a.len() > count_b.len() {
            Ordering::Less
        } else {
            Ordering::Greater
        }
    }
}

fn sort_by_chars(a: &str, b: &str, char_sorter: Box<dyn Fn(Option<(char, char)>) -> Ordering>) -> Ordering {
    let mut pairs = a.chars().zip(b.chars());

    let mut res = char_sorter(pairs.next());
    while res == Ordering::Equal {
        res = char_sorter(pairs.next());
    }

    res
}

fn sort_char_with_joker(pair: Option<(char, char)>) -> Ordering {
    if pair.is_none() {
        return Ordering::Equal;
    }
    let (a, b) = pair.unwrap();
    if a == b {
        Ordering::Equal
    } else {
        match a {
            'A' => Ordering::Greater,
            'K' => if b == 'A' { Ordering::Less } else { Ordering::Greater },
            'Q' => if ['A', 'K'].contains(&b) { Ordering::Less } else { Ordering::Greater },
            'T' => if ['A', 'K', 'Q'].contains(&b) { Ordering::Less } else { Ordering::Greater },
            '9' => if ['A', 'K', 'Q', 'T'].contains(&b) { Ordering::Less } else { Ordering::Greater },
            '8' => if ['A', 'K', 'Q', 'T', '9'].contains(&b) { Ordering::Less } else { Ordering::Greater },
            '7' => if ['J', '2', '3', '4', '5', '6'].contains(&b) { Ordering::Greater } else { Ordering::Less },
            '6' => if ['J', '2', '3', '4', '5'].contains(&b) { Ordering::Greater } else { Ordering::Less },
            '5' => if ['J', '2', '3', '4'].contains(&b) { Ordering::Greater } else { Ordering::Less },
            '4' => if ['J', '2', '3'].contains(&b) { Ordering::Greater } else { Ordering::Less },
            '3' => if ['J', '2'].contains(&b) { Ordering::Greater } else { Ordering::Less },
            '2' => if b == 'J' { Ordering::Greater } else { Ordering::Less },
            'J' => Ordering::Less,
            _ => unreachable!()
        }
    }
}

fn sort_char(pair: Option<(char, char)>) -> Ordering {
    if pair.is_none() {
        return Ordering::Equal;
    }
    let (a, b) = pair.unwrap();
    if a == b {
        Ordering::Equal
    } else {
        match a {
            'A' => Ordering::Greater,
            'K' => if b == 'A' { Ordering::Less } else { Ordering::Greater },
            'Q' => if ['A', 'K'].contains(&b) { Ordering::Less } else { Ordering::Greater },
            'J' => if ['A', 'K', 'Q'].contains(&b) { Ordering::Less } else { Ordering::Greater },
            'T' => if ['A', 'K', 'Q', 'J'].contains(&b) { Ordering::Less } else { Ordering::Greater },
            '9' => if ['A', 'K', 'Q', 'J', 'T'].contains(&b) { Ordering::Less } else { Ordering::Greater },
            '8' => if ['2', '3', '4', '5', '6', '7'].contains(&b) { Ordering::Greater } else { Ordering::Less },
            '7' => if ['2', '3', '4', '5', '6'].contains(&b) { Ordering::Greater } else { Ordering::Less },
            '6' => if ['2', '3', '4', '5'].contains(&b) { Ordering::Greater } else { Ordering::Less },
            '5' => if ['2', '3', '4'].contains(&b) { Ordering::Greater } else { Ordering::Less },
            '4' => if ['2', '3'].contains(&b) { Ordering::Greater } else { Ordering::Less },
            '3' => if b == '2' { Ordering::Greater } else { Ordering::Less },
            '2' => Ordering::Less,
            _ => unreachable!()
        }
    }
}

fn is_three_of_a_kind(chars: HashMap<char, i32>) -> bool {
    chars.iter().any(|(_, i)| *i == 3)
}

fn is_four_of_a_kind(chars: HashMap<char, i32>) -> bool {
    chars.iter().any(|(_, i)| *i == 4)
}

fn char_counts(s: &str) -> HashMap<char, i32> {
    s.chars()
        .fold(HashMap::new(), |mut map, c| {
            *map.entry(c).or_insert(0) += 1;
            map
        })
}

fn char_counts_with_joker(s: &str) -> HashMap<char, i32> {
    let mut map = s.chars()
        .fold(HashMap::new(), |mut map, c| {
            *map.entry(c).or_insert(0) += 1;
            map
        });

    if map.len() > 1 {
        let joker_count = map.remove(&'J');
        if joker_count.is_some() {
            let count = joker_count.unwrap();
            for (_, i) in map.iter_mut() {
                *i += count
            }
        }
    }

    map
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1("32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483"), 6440)
    }

    #[test]
    fn sample2() {
        assert_eq!(part2("32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483"), 5905)
    }
}
