use itertools::Itertools;

#[aoc(day12, part1)]
pub fn part1(input: &str) -> i64 {
    let mut arrangements = 0;
    for line in input.lines() {
        let (mut chars, damaged) = parse_line(line);
        arrangements += find_arrangements(&mut chars, damaged)
    }

    arrangements
}

fn find_arrangements(chars: &mut Vec<char>, damaged: Vec<u32>) -> i64 {
    if chars.last() == Some(&'.') {
        chars.pop();
    }
    let mut modified_chars = Vec::with_capacity(chars.len() + 1);
    modified_chars.push('.');
    modified_chars.extend(chars.iter());
    let working_size = modified_chars.len() + 1;

    let (mut old_state, mut new_state) = (vec![0; working_size], vec![0; working_size]);
    old_state[0] = 1;

    for i in 1..modified_chars.len() {
        if modified_chars[i] != '#' { old_state[i] = 1; } else { break; }
    }

    for cnt in damaged {
        let cnt = cnt as usize;
        let mut grp = 0;
        for (i, &c) in modified_chars.iter().enumerate() {
            if c == '.' { grp = 0; } else { grp += 1; }
            if c != '#' {
                new_state[i + 1] += new_state[i];
            }
            if grp >= cnt && modified_chars[i - cnt] != '#' {
                new_state[i + 1] += old_state[i - cnt];
            }
        }
        old_state.iter_mut().for_each(|x| *x = 0);
        (old_state, new_state) = (new_state, old_state);
    }

    old_state[working_size - 1]
}

fn parse_line(line: &str) -> (Vec<char>, Vec<u32>) {
    let split = line.split_once(' ').unwrap();

    (split.0.chars().collect_vec().into(), split.1.split(',').filter_map(|num| num.parse::<u32>().ok()).collect_vec())
}

#[aoc(day12, part2)]
pub fn part2(input: &str) -> i64 {
    let mut arrangements = 0;
    for line in input.lines() {
        let (mut chars, damaged) = parse_line(line);
        chars.push('?');
        chars = chars.repeat(5);
        chars.pop();
        arrangements += find_arrangements(&mut chars, damaged.repeat(5))
    }

    arrangements
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1("???.### 1,1,3"), 1)
    }

    #[test]
    fn sample2() {
        assert_eq!(part1(".??..??...?##. 1,1,3"), 4)
    }
    #[test]
    fn sample3() {
        assert_eq!(part1("?#?#?#?#?#?#?#? 1,3,1,6"), 1)
    }
    #[test]
    fn sample4() {
        assert_eq!(part1("????.#...#... 4,1,1"), 1)
    }
    #[test]
    fn sample5() {
        assert_eq!(part1("????.######..#####. 1,6,5"), 4)
    }
    #[test]
    fn sample6() {
        assert_eq!(part1("?###???????? 3,2,1"), 10)
    }
    #[test]
    fn sample7() {
        assert_eq!(part1("???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1"), 21)
    }

    #[test]
    fn sample8() {
        assert_eq!(part2("???.### 1,1,3"), 1)
    }

    #[test]
    fn sample9() {
        assert_eq!(part2(".??..??...?##. 1,1,3"), 16384)
    }
    #[test]
    fn sample10() {
        assert_eq!(part2("?#?#?#?#?#?#?#? 1,3,1,6"), 1)
    }
    #[test]
    fn sample11() {
        assert_eq!(part2("????.#...#... 4,1,1"), 16)
    }
    #[test]
    fn sample12() {
        assert_eq!(part2("????.######..#####. 1,6,5"), 2500)
    }
    #[test]
    fn sample13() {
        assert_eq!(part2("?###???????? 3,2,1"), 506250)
    }
    #[test]
    fn sample14() {
        assert_eq!(part2("???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1"), 525152)
    }
}
