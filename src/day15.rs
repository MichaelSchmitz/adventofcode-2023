use itertools::Itertools;

#[aoc(day15, part1)]
pub fn part1(input: &str) -> u32 {
    let mut hash_sum = 0;
    for instruction in input.split(',') {
        hash_sum += calc_hash(instruction);
    }

    hash_sum
}

fn calc_hash(input: &str) -> u32 {
    let mut hash = 0;
    for c in input.chars() {
        match c {
            ',' => continue,
            _ => hash = ((hash + c as u32) * 17) % 256
        }
    }

    hash
}

#[aoc(day15, part2)]
pub fn part2(input: &str) -> usize {
    let mut boxes: [Vec<(String, String)>;256] = std::array::from_fn(|_| Vec::new());
    for instruction in input.split(',') {
        let mut chars = instruction.chars();
        let label = chars.peeking_take_while(|c| *c != '=' && *c != '-').join("");
        let op = chars.next().unwrap();
        let id = calc_hash(label.as_str()) as usize;
        let b = &mut boxes[id];
        let lens = b.iter().find_position(|l| l.0 == label);
        if lens.is_some() {
            if op == '=' {
                let focal = chars.next().unwrap();
                let index = lens.unwrap().0;
                b.remove(index);
                b.insert(index, (label, focal.to_string()));
            }
            else {
                b.remove(lens.unwrap().0);
            }
        }
        else {
            if op == '=' {
                let focal = chars.next().unwrap();
                b.push((label, focal.to_string()));
            }
        }
    }

    let mut power = 0;
    for (i, b) in boxes.iter().enumerate() {
        for (j, lens) in b.iter().enumerate() {
            power += (1 + i) * (j + 1) * lens.1.parse::<usize>().unwrap();
        }
    }

    power
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample0() {
        assert_eq!(part1("HASH"), 52)
    }
    #[test]
    fn sample1() {
        assert_eq!(part1("rn=1"), 30)
    }

    #[test]
    fn sample2() {
        assert_eq!(part1("cm-"), 253)
    }

    #[test]
    fn sample3() {
        assert_eq!(part1("qp=3"), 97)
    }

    #[test]
    fn sample4() {
        assert_eq!(part1("cm=2"), 47)
    }

    #[test]
    fn sample5() {
        assert_eq!(part1("qp-"), 14)
    }

    #[test]
    fn sample6() {
        assert_eq!(part1("pc=4"), 180)
    }

    #[test]
    fn sample7() {
        assert_eq!(part1("ot=9"), 9)
    }

    #[test]
    fn sample8() {
        assert_eq!(part1("ab=5"), 197)
    }

    #[test]
    fn sample9() {
        assert_eq!(part1("pc-"), 48)
    }

    #[test]
    fn sample10() {
        assert_eq!(part1("pc=6"), 214)
    }


    #[test]
    fn sample11() {
        assert_eq!(part1("ot=7"), 231)
    }

    #[test]
    fn sample12() {
        assert_eq!(part1("rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7"), 1320)
    }

    #[test]
    fn sample13() {
        assert_eq!(part2("rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7"), 145)
    }
}
