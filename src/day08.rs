use std::collections::HashMap;
use num_integer::Integer;

pub fn parse_map(input: &str) -> (Vec<char>, HashMap<&str, (&str, &str)>) {
    let mut map: HashMap<&str, (&str, &str)> = HashMap::new();
    let mut lines = input.lines();
    let instructions: Vec<char> = lines.next().unwrap().chars().collect();
    lines.next();
    for line in lines {
        let split: Vec<&str> = line.split(" = ").collect();
        let targets: Vec<&str> = split[1].split(", ").collect();
        let mut left = targets[0].chars();
        left.next();
        let mut right = targets[1].chars();
        right.next_back();
        map.insert(split[0].trim(), (left.as_str(), right.as_str()));
    }

    (instructions, map)
}

#[aoc(day8, part1)]
pub fn part1(input: &str) -> usize {
    let parsed = parse_map(input);

    solve_single("AAA", &parsed.0, &parsed.1, Box::new(single_end))
}

fn single_end(s: &str) -> bool {
    s == "ZZZ"
}

fn multi_end(s: &str) -> bool {
    s.ends_with('Z')
}

fn solve_single(start: &str, directions: &Vec<char>, instructions: &HashMap<&str, (&str, &str)>, is_end_node: Box<dyn Fn(&str) -> bool>) -> usize {
    let mut current_node = start;
    let mut steps = 0;
    let modulo = directions.len();
    while !is_end_node(current_node) {
        let step = directions[steps % modulo];
        steps += 1;

        if step == 'R' {
            current_node = instructions[current_node].1
        }
        else if step == 'L' {
            current_node = instructions[current_node].0
        }
        else {
            unreachable!()
        }
    }

    steps
}

#[aoc(day8, part2)]
pub fn part2(input: &str) -> usize {
    let parsed = parse_map(input);

    let start_nodes: Vec<_> = parsed.1.keys().filter(|key| key.ends_with('A')).collect();

    let mut steps_per_node: Vec<usize> = Vec::new();

    for start_node in start_nodes {
        steps_per_node.push(solve_single(start_node, &parsed.0, &parsed.1, Box::new(multi_end)))
    }

    lcm_vec(steps_per_node)
}

fn lcm_vec(vec: Vec<usize>) -> usize {
    if vec.len() == 1 {
        vec[0]
    }
    else {
        let mut new_vec = vec.clone();
        let num = new_vec.pop();
        lcm_vec(new_vec).lcm(&num.unwrap())
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1("RL

        AAA = (BBB, CCC)
        BBB = (DDD, EEE)
        CCC = (ZZZ, GGG)
        DDD = (DDD, DDD)
        EEE = (EEE, EEE)
        GGG = (GGG, GGG)
        ZZZ = (ZZZ, ZZZ)"), 2)
    }

    #[test]
    fn sample2() {
        assert_eq!(part1("LLR

        AAA = (BBB, BBB)
        BBB = (AAA, ZZZ)
        ZZZ = (ZZZ, ZZZ)"), 6)
    }

    #[test]
    fn sample3() {
        assert_eq!(part2("LR

        11A = (11B, XXX)
        11B = (XXX, 11Z)
        11Z = (11B, XXX)
        22A = (22B, XXX)
        22B = (22C, 22C)
        22C = (22Z, 22Z)
        22Z = (22B, 22B)
        XXX = (XXX, XXX)"), 6)
    }
}