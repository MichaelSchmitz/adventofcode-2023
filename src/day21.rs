use std::collections::{HashMap, HashSet};

#[aoc(day21, part1)]
pub fn part1(input: &str) -> usize {
    let (map, start) = gen_map(input);
    let mut current = HashSet::new();
    current.insert(start);
    solve_steps(&map, 64, current)
}

fn solve_steps(map: &HashMap<(i32, i32), bool>, steps: u32, input: HashSet<(i32, i32)>) -> usize {
    let mut current: HashSet<(i32, i32)> = input.clone();
    let mut next: HashSet<(i32, i32)> = HashSet::new();

    for _ in 0..steps {
        for (x, y) in current.clone() {
            for neighbour in get_neighbours(x , y) {
                let pos = get_modulo(neighbour);
                let n = map.get(&pos);
                if n.is_some_and(|&v| !v) {
                    next.insert(neighbour);
                }
            }
        }
        current = next;
        next = HashSet::new();
    }

    current.len()
}

fn get_modulo(pos: (i32, i32)) -> (i32, i32) {
    let mut x = pos.0;
    let mut y = pos.1;
    while x < 0 {
        x += 131;
    }
    while y < 0 {
        y += 131;
    }
    x = x % 131;
    y = y % 131;

    (x, y)
}

fn gen_map(input: &str) -> (HashMap<(i32, i32), bool>, (i32, i32)) {
    let mut map: HashMap<(i32, i32), bool> = HashMap::new();
    let mut start = (0, 0);
    for (y, line) in input.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            if c == 'S' {
                start = (x as i32, y as i32);
            }
            map.insert((x as i32, y as i32), c == '#');
        }
    }
    (map, start)
}

fn get_neighbours(x: i32, y: i32) -> Vec<(i32, i32)> {
    vec![(x +1, y), (x, y + 1), (x - 1, y), (x, y - 1)]
}

#[aoc(day21, part2)]
pub fn part2(input: &str) -> usize {
    let (map, start) = gen_map(input);
    let mut current = HashSet::new();
    current.insert(start);

    let mut sequence = Vec::new();
    let steps = 26501365usize;
    let grid_size = 131;
    let remainder = steps % grid_size;
    let grids = steps / grid_size;
    for i in 0..3 {
        sequence.push(solve_steps(&map, (remainder + i * grid_size) as u32, current.clone()));
    }
    let c = sequence[0];
    let a = (sequence[2] - c - 2*( sequence[1] - c)) / 2;
    let b = sequence[1] - c - a;

    a * (grids * grids) + b * grids + c
}

#[cfg(test)]
mod tests {
    use super::*;

    fn call() -> usize {
        let (map, start) = gen_map("...........
.....###.#.
.###.##..#.
..#.#...#..
....#.#....
.##..S####.
.##..#...#.
.......##..
.##.#.####.
.##..##.##.
...........");
        let mut current = HashSet::new();
        current.insert(start);
        solve_steps(&map, 6, current)
    }

    #[test]
    fn sample1() {
        assert_eq!(call(), 16)
    }
}
