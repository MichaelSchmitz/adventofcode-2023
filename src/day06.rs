use std::ops::Range;
use regex::Regex;

#[aoc(day6, part1)]
pub fn part1(input: &str) -> u64 {
    let mut lines = input.lines();
    let time_matches = parse_line(lines.next().unwrap());
    let dist_matches = parse_line(lines.next().unwrap());

    let mut count = 1;
    for (time, dist) in time_matches.iter().zip(dist_matches.iter()) {
        count *= divide_and_conquer(1..*time, *time as u64, *dist as u64);
    }

    count
}

fn divide_and_conquer(range: Range<u64>, total: u64, dist: u64) -> u64 {
    if calc_dist(range.start, total) > dist && calc_dist(range.end, total) > dist {
        range.end - range.start + 1
    }
    else if range.start >= range.end {
        0
    }
    else {
        let left = range.start..(range.start + range.end)/2;
        let right = (range.start + range.end)/2+1..range.end;

        divide_and_conquer(left, total, dist) + divide_and_conquer(right, total, dist)
    }
}

fn calc_dist(hold: u64, total: u64) -> u64 {
    (total - hold) * hold
}

fn parse_line(line: &str) -> Vec<u64> {
    let line_regex = Regex::new(r"\d+").unwrap();

    line_regex.find_iter(line).map(|m| m.as_str().parse::<u64>().unwrap()).collect()
}

#[aoc(day6, part2)]
pub fn part2(input: &str) -> u64 {
    let mut lines = input.lines();
    let time = parse_single_race(lines.next().unwrap());
    let dist = parse_single_race(lines.next().unwrap());

    divide_and_conquer(1..time, time, dist)
}

fn parse_single_race(line: &str) -> u64 {
    let nums = line.split(':').collect::<Vec<&str>>()[1];
    let mut str = nums.to_string();
    str.retain(|c| !c.is_whitespace());

    str.parse::<u64>().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1("Time:      7  15   30
Distance:  9  40  200"), 288)
    }

    #[test]
    fn sample2() {
        assert_eq!(part2("Time:      7  15   30
Distance:  9  40  200"), 71503)
    }
}