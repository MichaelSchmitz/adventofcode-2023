#[aoc(day1, part1)]
pub fn part1(input: &str) -> u32 {
    let mut nums: Vec<u32> = Vec::new();
    for line in input.lines() {
        let mut first = 'a';
        let mut last = 'a';
        for char in line.chars() {
            if char.is_numeric() {
                if first == 'a'
                {
                    first = char;
                }
                last = char;
            }
        }
        nums.push(format!("{}{}", first, last).parse::<u32>().unwrap())
    }

    nums.iter().sum()
}

#[aoc(day1, part2)]
pub fn part2(input: &str) -> u32 {
    let mut nums: Vec<u32> = Vec::new();
    for line in input.lines() {
        let mut digits = (0..line.len()).filter_map(|i| starts_with_digit(&line[i..]));
        let first = digits.next().unwrap();
        let last = digits.last().unwrap_or(first);

        nums.push(first * 10 + last)
    }

    nums.iter().sum()
}

fn starts_with_digit(line: &str) -> Option<u32> {
    let c = line.chars().next()?;
    if let r @ Some(_) = c.to_digit(10) {
        return r;
    }

    for (spelled, value) in [
        ("one", 1),
        ("two", 2),
        ("three", 3),
        ("four", 4),
        ("five", 5),
        ("six", 6),
        ("seven", 7),
        ("eight", 8),
        ("nine", 9),
    ] {
        if line.starts_with(spelled) {
            return Some(value);
        }
    }

    None
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1("1abc2"), 12)
    }
    #[test]
    fn sample2() {
        assert_eq!(part1("pqr3stu8vwx"), 38)
    }
    #[test]
    fn sample3() {
        assert_eq!(part1("a1b2c3d4e5f"), 15)
    }
    #[test]
    fn sample4() {
        assert_eq!(part1("treb7uchet"), 77)
    }
    #[test]
    fn sample5() {
        assert_eq!(part1("1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet"), 142)
    }

    #[test]
    fn sample6() {
        assert_eq!(part2("two1nine"), 29)
    }
    #[test]
    fn sample7() {
        assert_eq!(part2("eightwothree"), 83)
    }
    #[test]
    fn sample8() {
        assert_eq!(part2("abcone2threexyz"), 13)
    }
    #[test]
    fn sample9() {
        assert_eq!(part2("xtwone3four"), 24)
    }
    #[test]
    fn sample10() {
        assert_eq!(part2("4nineeightseven2"), 42)
    }
    #[test]
    fn sample11() {
        assert_eq!(part2("zoneight234"), 14)
    }
    #[test]
    fn sample12() {
        assert_eq!(part2("7pqrstsixteen"), 76)
    }
    #[test]
    fn sample13() {
        assert_eq!(part2("two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen"), 281)
    }
}