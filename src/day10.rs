use std::collections::{HashMap, HashSet};
use itertools::Itertools;

type Map = HashMap<(i32, i32), char>;

#[derive(Debug, Clone, Copy)]
enum Dir {
    North, East, West, South
}

#[aoc_generator(day10)]
pub fn gen_map(input: &str) -> Map {
    let mut map = Map::new();

    for (y, line) in input.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            map.insert((x as i32, y as i32), c);
        }
    }

    map
}

#[aoc(day10, part1)]
pub fn part1(input: &Map) -> usize {
    let (x_start, y_start) = input.iter().find(|(_, v)| **v == 'S').unwrap().0;
    find_longest_path(*x_start, *y_start, input).len() / 2
}

fn find_start_next(start_x: i32, start_y: i32, map: &Map) -> (i32, i32, Dir) {
    let mut pipe = map.get(&(start_x, start_y + 1));
    if pipe.is_some_and(|c| *c == '|' || *c == 'J' || *c == 'L') {
        return (start_x, start_y + 1, Dir::South)
    }
    pipe = map.get(&(start_x, start_y - 1));
    if pipe.is_some_and(|c| *c == '|' || *c == 'F' || *c == '7') {
        return (start_x, start_y - 1, Dir::North)
    }
    pipe = map.get(&(start_x + 1, start_y));
    if pipe.is_some_and(|c| *c == '-' || *c == 'J' || *c == '7') {
        return (start_x + 1, start_y, Dir::East)
    }
    pipe = map.get(&(start_x - 1, start_y));
    if pipe.is_some_and(|c| *c == '-' || *c == 'F' || *c == 'L') {
        return (start_x - 1, start_y, Dir::West)
    }
    unreachable!()
}

fn find_longest_path(start_x: i32, start_y: i32, map: &Map) -> Vec<(i32, i32)> {
    let mut steps: Vec<(i32, i32)> = Vec::new();
    steps.push((start_x, start_y));
    let (mut current_x, mut current_y, mut direction) = find_start_next(start_x, start_y, map);

    let mut pipe = map.get(&(current_x, current_y)).unwrap();
    while *pipe != 'S' {
        steps.push((current_x, current_y));
        match (direction, pipe) {
            (Dir::South, '|') => current_y += 1,
            (Dir::North, '|') => current_y -= 1,
            (Dir::West, '-') => current_x -= 1,
            (Dir::East, '-') => current_x += 1,
            (Dir::South, 'L') => {
                current_x += 1;
                direction = Dir::East;
            },
            (Dir::West, 'L') => {
                current_y -= 1;
                direction = Dir::North;
            },
            (Dir::South, 'J') => {
                current_x -= 1;
                direction = Dir::West;
            },
            (Dir::East, 'J') => {
                current_y -= 1;
                direction = Dir::North;
            },
            (Dir::East, '7') => {
                current_y += 1;
                direction = Dir::South;
            },
            (Dir::North, '7') => {
                current_x -= 1;
                direction = Dir::West;
            },
            (Dir::West, 'F') => {
                current_y += 1;
                direction = Dir::South;
            },
            (Dir::North, 'F') => {
                current_x += 1;
                direction = Dir::East;
            },
            _ => {
                println!("{}, {}, {}, {:?}", pipe, current_x, current_y, direction);
                unreachable!()
            }
        }

        pipe = map.get(&(current_x, current_y)).unwrap();

    }

    steps
}

#[aoc(day10, part2)]
pub fn part2(input: &Map) -> usize {
    let (x_start, y_start) = input.iter().find(|(_, v)| **v == 'S').unwrap().0;

    let s_char = find_s_char(*x_start, *y_start, input);
    let pipes: HashSet<(i32, i32)> = HashSet::from_iter(find_longest_path(*x_start, *y_start, input).iter().cloned());
    let reduced_input = input.iter().filter(|(k, _)| pipes.contains(k)).collect_vec();
    let non_pipes = input.keys().filter(|k| !pipes.contains(k)).collect_vec();
    let mut enclosed = 0;

    for point in non_pipes {
        // crossing number polygon inner areas
        let filtered = reduced_input.iter().filter(|(k,v)| k.0 > point.0 && k.1 == point.1 && (is_vertical(**v) || (**v == 'S' && is_vertical(s_char)))).collect_vec();

        if filtered.len() % 2 == 1 {
            enclosed += 1;
        }
    }

    enclosed
}

fn is_vertical(c: char) -> bool {
    c == '|' || c == 'F' || c == '7'
}

fn find_s_char(x: i32, y: i32, map: &Map) -> char {
    let mut pipe = map.get(&(x, y + 1));
    let mut has_south = false;
    let mut has_north = false;
    let mut has_west = false;
    let mut has_east = false;
    if pipe.is_some_and(|c| *c == '|' || *c == 'J' || *c == 'L') {
        has_south = true;
    }
    pipe = map.get(&(x, y - 1));
    if pipe.is_some_and(|c| *c == '|' || *c == 'F' || *c == '7') {
        has_north = true;
    }
    pipe = map.get(&(x + 1, y));
    if pipe.is_some_and(|c| *c == '-' || *c == 'J' || *c == '7') {
        has_east = true;
    }
    pipe = map.get(&(x - 1, y));
    if pipe.is_some_and(|c| *c == '-' || *c == 'F' || *c == 'L') {
        has_west = true;
    }

    if has_south && has_north {
        '|'
    }
    else if has_south && has_west {
        '7'
    }
    else if has_south && has_east {
        'F'
    }
    else if has_north && has_west {
        'J'
    }
    else if has_north && has_east {
        'L'
    }
    else if has_east && has_west {
        '-'
    }
    else {
        unreachable!()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1(&gen_map(".....
    .S-7.
    .|.|.
    .L-J.
    .....")), 4)
    }

    #[test]
    fn sample2() {
        assert_eq!(part1(&gen_map("-L|F7
7S-7|
L|7||
-L-J|
L|-JF")), 4)
    }

    #[test]
    fn sample3() {
        assert_eq!(part1(&gen_map("..F7.
.FJ|.
SJ.L7
|F--J
LJ...")), 8)
    }

    #[test]
    fn sample4() {
        assert_eq!(part1(&gen_map("7-F7-
.FJ|7
SJLL7
|F--J
LJ.LJ")), 8)
    }

    #[test]
    fn sample5() {
        assert_eq!(part2(&gen_map("...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........")), 4)
    }

    #[test]
    fn sample6() {
        assert_eq!(part2(&gen_map(".F----7F7F7F7F-7....
.|F--7||||||||FJ....
.||.FJ||||||||L7....
FJL7L7LJLJ||LJ.L-7..
L--J.L7...LJS7F-7L7.
....F-J..F7FJ|L7L7L7
....L7.F7||L7|.L7L7|
.....|FJLJ|FJ|F7|.LJ
....FJL-7.||.||||...
....L---J.LJ.LJLJ...")), 8)
    }

    #[test]
    fn sample7() {
        assert_eq!(part2(&gen_map("FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJ7F7FJ-
L---JF-JLJ.||-FJLJJ7
|F|F-JF---7F7-L7L|7|
|FFJF7L7F-JF7|JL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L")), 10)
    }
}
