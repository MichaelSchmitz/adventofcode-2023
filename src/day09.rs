use itertools::Itertools;

#[aoc(day9, part1)]
pub fn part1(input: &str) -> i32 {
    let mut result: i32 = 0;
    for line in input.lines() {
        result += predict_next_val(parse_line(line));
    }

    result
}

fn predict_next_val(vec: Vec<i32>) -> i32 {
    if vec.len() == 0
    {
        return 0;
    }
    let mut diff_vec: Vec<i32> = Vec::new();
    for (a, b) in vec.iter().tuple_windows() {
        diff_vec.push(b - a);
    }

    predict_next_val(diff_vec) + vec.last().unwrap()
}

fn predict_last_val(vec: Vec<i32>) -> i32 {
    if vec.len() == 0
    {
        return 0;
    }
    let mut diff_vec: Vec<i32> = Vec::new();
    for (a, b) in vec.iter().tuple_windows() {
        diff_vec.push(b - a);
    }

    vec.first().unwrap() - predict_last_val(diff_vec)
}

fn parse_line(line: &str) -> Vec<i32> {
    line.split(' ').collect::<Vec<&str>>().iter().filter_map(|n| n.parse::<i32>().ok()).collect::<Vec<i32>>()
}

#[aoc(day9, part2)]
pub fn part2(input: &str) -> i32 {
    let mut result: i32 = 0;
    for line in input.lines() {
        result += predict_last_val(parse_line(line));
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1("0 3 6 9 12 15"), 18)
    }
    #[test]
    fn sample2() {
        assert_eq!(part1("1 3 6 10 15 21"), 28)
    }
    #[test]
    fn sample3() {
        assert_eq!(part1("10 13 16 21 30 45"), 68)
    }
    #[test]
    fn sample4() {
        assert_eq!(part1("0 3 6 9 12 15
        1 3 6 10 15 21
        10 13 16 21 30 45"), 114)
    }

    #[test]
    fn sample5() {
        assert_eq!(part2("0 3 6 9 12 15"), -3)
    }
    #[test]
    fn sample6() {
        assert_eq!(part2("1 3 6 10 15 21"), 0)
    }
    #[test]
    fn sample7() {
        assert_eq!(part2("10 13 16 21 30 45"), 5)
    }
    #[test]
    fn sample8() {
        assert_eq!(part2("0 3 6 9 12 15
        1 3 6 10 15 21
        10 13 16 21 30 45"), 2)
    }
}