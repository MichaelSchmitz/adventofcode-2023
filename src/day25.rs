use graphrs::{Edge, Graph, GraphSpecs};
use graphrs::algorithms::community::louvain;

#[derive(Debug)]
struct Component{
    connected: Vec<String>
}

#[aoc(day25, part1)]
pub fn part1(input: &str) -> usize {
    let mut graph = Graph::<&str, ()>::new(GraphSpecs::undirected_create_missing());
    for line in input.lines() {
        let (a, bs) = line.split_once(": ").unwrap();
        for b in bs.split_whitespace() {
            graph.add_edge(Edge::new(a, b)).expect("Edge should be added");
        }
    }

    let res = louvain::louvain_partitions(&graph, false, Some(0f64), Some(4f64), None).unwrap();

    res[0][0].len() * res[0][1].len()
}

#[aoc(day25, part2)]
pub fn part2(_input: &str) -> usize {
    1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample1() {
        assert_eq!(part1("jqt: rhn xhk nvd
rsh: frs pzl lsr
xhk: hfx
cmg: qnr nvd lhk bvb
rhn: xhk bvb hfx
bvb: xhk hfx
pzl: lsr hfx nvd
qnr: nvd
ntq: jqt hfx bvb xhk
nvd: lhk
lsr: lhk
rzs: qnr cmg lsr rsh
frs: qnr lhk lsr"), 54)
    }

    #[test]
    fn sample2() {
        assert_eq!(part2(""), 1)
    }
}
